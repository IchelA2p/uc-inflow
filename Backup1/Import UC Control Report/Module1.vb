﻿Imports System.IO
Imports System.Data.OleDb

Module Module1

    Dim CtrlRptFilename As String = ""
    Dim ctrlrptPath As String = ""
    Dim ctrlrptstat As String = ""
    Dim errCount As Integer = 0

#Region "Old RESI Activation"
    'Private Sub ImportRESIActivation()

    '    Dim cls As New clsConnection
    '    Dim conn As OleDbConnection
    '    Dim dta As OleDbDataAdapter
    '    Dim dts As DataSet
    '    Dim dtb As New DataTable

    '    'dtb.Columns.Add("id", GetType(Integer))
    '    dtb.Columns.Add("Property ID", GetType(String))
    '    dtb.Columns.Add("State", GetType(String))
    '    dtb.Columns.Add("User Name", GetType(String))
    '    dtb.Columns.Add("Boarded Date", GetType(String))
    '    dtb.Columns.Add("RSTRAT Date", GetType(String))
    '    dtb.Columns.Add("Client Status", GetType(String))
    '    dtb.Columns.Add("Utility Status", GetType(String))
    '    dtb.Columns.Add("Date Stamp/ As of Date", GetType(String))
    '    dtb.Columns.Add("Aging", GetType(String))
    '    dtb.Columns.Add("Electric Status", GetType(String))
    '    dtb.Columns.Add("Electric Service Provider", GetType(String))
    '    dtb.Columns.Add("Gas Status", GetType(String))
    '    dtb.Columns.Add("Gas Service Provider", GetType(String))
    '    dtb.Columns.Add("Holidays", GetType(String))


    '    Try
    '        conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
    '        dta = New OleDbDataAdapter("Select * From [RESI-Open Inventory$]", conn)
    '        dts = New DataSet

    '        Try
    '            dta.Fill(dts, "[RESI-Open Inventory$]")

    '            For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
    '                dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(1).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(2).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(3).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(4).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(5).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(6).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(7).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(8).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(9).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(10).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(11).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(12).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(13).ToString.Trim)
    '            Next

    '            Try
    '                cls.SQLBulkCopy("tbl_UC_CtrlRpt_RESIOpenInventory", dtb)
    '                cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_RESIOpenInventory where [Property ID] = ''")
    '            Catch ex As Exception

    '                errCount += 1
    '                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
    '            End Try
    '        Catch ex As Exception

    '            errCount += 1
    '            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '        End Try
    '    Catch ex As Exception

    '        errCount += 1
    '        'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '    End Try

    'End Sub

    'Private Sub ImportRESIWaterInv()

    '    Dim cls As New clsConnection
    '    Dim conn As OleDbConnection
    '    Dim dta As OleDbDataAdapter
    '    Dim dts As DataSet
    '    Dim dtb As New DataTable

    '    'dtb.Columns.Add("id", GetType(Integer))
    '    dtb.Columns.Add("Property ID", GetType(String))
    '    dtb.Columns.Add("State", GetType(String))
    '    dtb.Columns.Add("User Name", GetType(String))
    '    dtb.Columns.Add("Boarded Date", GetType(String))
    '    dtb.Columns.Add("RSTRAT/WCONFIRM Date", GetType(String))
    '    dtb.Columns.Add("Client Status", GetType(String))
    '    dtb.Columns.Add("Utility Status", GetType(String))
    '    dtb.Columns.Add("Date Stamp/ As of Date", GetType(String))
    '    dtb.Columns.Add("Aging", GetType(String))
    '    dtb.Columns.Add("Water Status", GetType(String))
    '    dtb.Columns.Add("Water Service Provider", GetType(String))


    '    Try
    '        conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
    '        dta = New OleDbDataAdapter("Select * From [Water Inventory$]", conn)
    '        dts = New DataSet

    '        Try
    '            dta.Fill(dts, "[Water Inventory$]")

    '            For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
    '                dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(1).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(2).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(3).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(4).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(5).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(6).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(7).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(8).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(9).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(10).ToString.Trim)
    '            Next

    '            Try
    '                cls.SQLBulkCopy("tbl_UC_CtrlRpt_WaterInventory", dtb)
    '                cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_WaterInventory where [Property ID] = ''")
    '            Catch ex As Exception

    '                errCount += 1
    '                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
    '            End Try
    '        Catch ex As Exception

    '            errCount += 1
    '            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '        End Try
    '    Catch ex As Exception

    '        errCount += 1
    '        'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '    End Try

    'End Sub

    'Private Sub ImportRESIWaterCompl()

    '    Dim cls As New clsConnection
    '    Dim conn As OleDbConnection
    '    Dim dta As OleDbDataAdapter
    '    Dim dts As DataSet
    '    Dim dtb As New DataTable

    '    'dtb.Columns.Add("id", GetType(Integer))
    '    dtb.Columns.Add("Property ID", GetType(String))
    '    dtb.Columns.Add("State", GetType(String))
    '    dtb.Columns.Add("User Name", GetType(String))
    '    dtb.Columns.Add("Boarded Date", GetType(String))
    '    dtb.Columns.Add("RSTRAT Date", GetType(String))
    '    dtb.Columns.Add("Client Status", GetType(String))
    '    dtb.Columns.Add("Utility Status", GetType(String))
    '    dtb.Columns.Add("Completions Date", GetType(String))
    '    dtb.Columns.Add("Aging", GetType(String))
    '    dtb.Columns.Add("Water Status", GetType(String))
    '    dtb.Columns.Add("Water Service Provider", GetType(String))
    '    dtb.Columns.Add("Water Date", GetType(String))
    '    dtb.Columns.Add("Status", GetType(String))
    '    dtb.Columns.Add("Exception Start Date", GetType(String))
    '    dtb.Columns.Add("Aging2", GetType(String))
    '    dtb.Columns.Add("Aging Excluding Exception Days", GetType(String))


    '    Try
    '        conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
    '        dta = New OleDbDataAdapter("Select * From [Water Compl$]", conn)
    '        dts = New DataSet

    '        Try
    '            dta.Fill(dts, "[Water Compl$]")

    '            For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
    '                dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(1).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(2).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(3).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(4).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(5).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(6).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(7).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(8).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(9).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(10).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(11).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(12).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(13).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(14).ToString.Trim, _
    '                             dts.Tables(0).Rows(i)(15).ToString.Trim)
    '            Next

    '            Try
    '                cls.SQLBulkCopy("tbl_UC_CtrlRpt_WaterCompl", dtb)
    '                cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_WaterCompl where [Property ID] = ''")
    '            Catch ex As Exception

    '                errCount += 1
    '                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
    '            End Try
    '        Catch ex As Exception

    '            errCount += 1
    '            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '        End Try
    '    Catch ex As Exception

    '        errCount += 1
    '        'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
    '    End Try

    'End Sub
#End Region

    Private Function GetLastCtrlRptImportDate() As String

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'LastCtrlRptModDate'")

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function GetLastCtrlRptModDate() As Date

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'LastCtrlRptImportDate'")

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0)
            Else
                Return ""
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function GetLastUtilityDataModDate() As String

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UtilityDataFileDate'")

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub CleanCtrlReport()

        Dim cls As New clsConnection
        Dim dqry As String = ""

        dqry += "delete from tbl_UC_CtrlRpt_Deactivation" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_WP" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_OpenInvCurrentDay" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_EventCompletionCurDay" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_EventCompletionPrevDay" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_PFCSumpPump" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_RESIOpenInventory" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_WaterInventory" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_WaterCompl" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_RESIDeactivation" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_RESIActivation" & vbCrLf
        dqry += "delete from tbl_UC_CtrlRpt_WaterPayoutUndercontract"

        cls.ExecuteQuery(dqry)
    End Sub

    Private Sub ImportREO_W()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("client_code", GetType(String))
        dtb.Columns.Add("property_code", GetType(String))
        dtb.Columns.Add("property_status", GetType(String))
        dtb.Columns.Add("utility_status", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utilities Raw Data-" & Now().ToString("MMddyyyy") & ".xlsb" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select [PropertyID], [REOStatus], [WaterStatus] From [REO W$] Where ([REOStatus] = 'O-SUBMIT' or [REOStatus] = 'SPND' or [REOStatus] = 'SPNDC' or [REOStatus] = 'SPNDX') And [occupancy_status] = 'Vacant'", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[REO W$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add("REO", dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_Undercontract", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_Undercontract where property_code = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception
                conn.Close()
                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

        conn.Close()

    End Sub

    Private Sub ImportRESI_W()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("client_code", GetType(String))
        dtb.Columns.Add("property_code", GetType(String))
        dtb.Columns.Add("property_status", GetType(String))
        dtb.Columns.Add("utility_status", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utilities Raw Data-" & Now().ToString("MMddyyyy") & ".xlsb" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select [PropertyID], [REOStatus], [Water_staus] From [RESI W$] Where ([REOStatus] = 'O-SUBMIT' or [REOStatus] = 'SPND' or [REOStatus] = 'SPNDC' or [REOStatus] = 'SPNDX') And [Occupancy Status] = 'Vacant'", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[RESI W$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add("RESI", dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_Undercontract", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_Undercontract where property_code = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                conn.Close()
                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

        conn.Close()

    End Sub

    Private Sub ImportDeactivation()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("PROP_ID", GetType(String))
        dtb.Columns.Add("Boarded Date", GetType(String))
        dtb.Columns.Add("Date", GetType(String))
        dtb.Columns.Add("Status", GetType(String))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("Utility-co ordinator", GetType(String))
        dtb.Columns.Add("Zip Code", GetType(String))
        dtb.Columns.Add("Electricity Status", GetType(String))
        dtb.Columns.Add("Electric Turn off Date", GetType(String))
        dtb.Columns.Add("Electric Service Provider", GetType(String))
        dtb.Columns.Add("Water Status", GetType(String))
        dtb.Columns.Add("Water Turn off Date", GetType(String))
        dtb.Columns.Add("Water Service Provider", GetType(String))
        dtb.Columns.Add("Gas Status", GetType(String))
        dtb.Columns.Add("Gas Turn off Date", GetType(String))
        dtb.Columns.Add("Gas Service Provider", GetType(String))
        dtb.Columns.Add("Status2", GetType(String))
        dtb.Columns.Add("E", GetType(String))
        dtb.Columns.Add("W", GetType(String))
        dtb.Columns.Add("G", GetType(String))
        dtb.Columns.Add("Total", GetType(String))
        dtb.Columns.Add("Completion Date", GetType(String))
        dtb.Columns.Add("Actual Aging", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("MONTH", GetType(String))
        dtb.Columns.Add("TAT", GetType(String))
        dtb.Columns.Add("TEAM", GetType(String))
        dtb.Columns.Add("Addition Date", GetType(String))
        dtb.Columns.Add("New Addtition", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Deactivation$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Deactivation$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(23).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(24).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(25).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(26).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(27).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(28).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(29).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_Deactivation", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_Deactivation where PROP_ID = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportWaterPayout()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("PODName", GetType(String))
        dtb.Columns.Add("PropertyID", GetType(String))
        dtb.Columns.Add("WSTRAT Date", GetType(String))
        dtb.Columns.Add("REOStatus", GetType(String))
        dtb.Columns.Add("PropertyUtilityStatus", GetType(String))
        dtb.Columns.Add("username", GetType(String))
        dtb.Columns.Add("WaterTurnOnDate", GetType(String))
        dtb.Columns.Add("WaterStatus", GetType(String))
        dtb.Columns.Add("Water_Status_Changed_Date", GetType(String))
        dtb.Columns.Add("Status", GetType(String))
        dtb.Columns.Add("Status 2", GetType(String))
        dtb.Columns.Add("Aging", GetType(String))
        dtb.Columns.Add("Aging2", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("Yesterday Completion", GetType(String))
        dtb.Columns.Add("Last Week Completion", GetType(String))
        dtb.Columns.Add("a", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Water Open Inventory$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Water Open Inventory$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_WP", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_WP where PropertyID = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub


    Private Sub ImportWaterPayoutUndercontract()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("Boarding Date", GetType(String))
        dtb.Columns.Add("Property ID", GetType(String))
        dtb.Columns.Add("WSTRAT Date", GetType(String))
        dtb.Columns.Add("REO Status", GetType(String))
        dtb.Columns.Add("WI #", GetType(String))
        dtb.Columns.Add("WI Status", GetType(String))
        dtb.Columns.Add("Line Item", GetType(String))
        dtb.Columns.Add("Turned On Date", GetType(String))
        dtb.Columns.Add("Water Status", GetType(String))
        dtb.Columns.Add("Last Update Date", GetType(String))
        dtb.Columns.Add("Water Service Provider Name", GetType(String))
        dtb.Columns.Add("WI Created Date", GetType(String))
        dtb.Columns.Add("Occupancy Status", GetType(String))
        dtb.Columns.Add("Qualifier", GetType(String))
        dtb.Columns.Add("Water Open/Closed", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Water Payout$] Where ([REO Status] = 'SPND' or [REO Status] = 'SPNDC' or [REO Status] = 'SPNDX') and [Occupancy Status] = 'Vacant'", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Water Payout$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_WaterPayoutUndercontract", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_WaterPayoutUndercontract where PropertyID = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub


    Private Sub Sample()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable


        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\atuprich\Documents\REOS FC Props.xls;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Sheet1$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Sheet1$]")

                Try
                    cls.SQLBulkCopy("Sheet1$", dts.Tables(0))
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub


    Private Sub ImportREOActivation()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("Property ID", GetType(String))
        dtb.Columns.Add("username", GetType(String))
        dtb.Columns.Add("Boarded Date", GetType(String))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("Pod Name", GetType(String))
        dtb.Columns.Add("WSTRAT Date", GetType(String))
        dtb.Columns.Add("REOStatus", GetType(String))
        dtb.Columns.Add("Utility Status", GetType(String))
        dtb.Columns.Add("Date Stamp/As of Date", GetType(String))
        dtb.Columns.Add("Aging", GetType(String))
        dtb.Columns.Add("System", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("Vendor Info", GetType(String))
        dtb.Columns.Add("Category", GetType(String))
        dtb.Columns.Add("Electricity", GetType(String))
        dtb.Columns.Add("Electricity Service Provider", GetType(String))
        dtb.Columns.Add("Gas", GetType(String))
        dtb.Columns.Add("Gas Service Provider", GetType(String))
        dtb.Columns.Add("Electricity_Stat", GetType(String))
        dtb.Columns.Add("Gas_Stat", GetType(String))
        dtb.Columns.Add("EG Ageing", GetType(String))
        dtb.Columns.Add("Parameter", GetType(String))
        dtb.Columns.Add("Aging2", GetType(String))
        dtb.Columns.Add("Final Status", GetType(String))
        dtb.Columns.Add("Exception Start Date", GetType(String))
        dtb.Columns.Add("Exception Category", GetType(String))
        dtb.Columns.Add("Zip Code", GetType(String))
        dtb.Columns.Add("Holiday", GetType(String))
        dtb.Columns.Add("PROPERTY STATUS", GetType(String))
        dtb.Columns.Add("Electricity Status", GetType(String))
        dtb.Columns.Add("Gas Status", GetType(String))
        dtb.Columns.Add("Elec Date", GetType(String))
        dtb.Columns.Add("Gas Date", GetType(String))
        dtb.Columns.Add("Elec Age", GetType(String))
        dtb.Columns.Add("Gas Age", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Open Inventory - Current Day$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Open Inventory - Current Day$]")

                For i As Integer = 1 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(22).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(23).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(24).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(25).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(26).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(27).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(28).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(29).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(30).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(31).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(32).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(33).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_OpenInvCurrentDay", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_OpenInvCurrentDay where [Property ID] = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportREOActivationCurDay()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        dtb.Columns.Add("Asset wise", GetType(String))
        dtb.Columns.Add("Co-Ordinator", GetType(String))
        dtb.Columns.Add("Boarded Date", GetType(String))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("Team", GetType(String))
        dtb.Columns.Add("WSTRAT", GetType(String))
        dtb.Columns.Add("REO Status", GetType(String))
        dtb.Columns.Add("Utility Status", GetType(String))
        dtb.Columns.Add("Turn On Date", GetType(String))
        dtb.Columns.Add("Aging", GetType(String))
        dtb.Columns.Add("System", GetType(String))
        dtb.Columns.Add("Vendor Info", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("Category", GetType(String))
        dtb.Columns.Add("Out of REO", GetType(String))
        dtb.Columns.Add("Electricity Status", GetType(String))
        dtb.Columns.Add("Electricity Turn on Date", GetType(String))
        dtb.Columns.Add("Gas Status", GetType(String))
        dtb.Columns.Add("Gas Turn On Date", GetType(String))
        dtb.Columns.Add("Exception START Date", GetType(String))
        dtb.Columns.Add("Exception Category", GetType(String))
        dtb.Columns.Add("Previous Status of Exception", GetType(String))
        dtb.Columns.Add("Aging2", GetType(String))
        dtb.Columns.Add("Actual Aging", GetType(String))
        dtb.Columns.Add("SLA", GetType(String))
        dtb.Columns.Add("Elec Age", GetType(String))
        dtb.Columns.Add("Gas Age", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Event Completion(MTD)-Cur Day$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Event Completion(MTD)-Cur Day$]")

                For i As Integer = 1 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(22).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(23).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(24).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(25).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(26).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_EventCompletionCurDay", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_EventCompletionCurDay where [Asset wise] = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportREOActivationPrevDay()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        dtb.Columns.Add("Property ID", GetType(String))
        dtb.Columns.Add("Co-Ordinator", GetType(String))
        dtb.Columns.Add("[Boarding Date]", GetType(String))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("Team", GetType(String))
        dtb.Columns.Add("WSTRAT", GetType(String))
        dtb.Columns.Add("REO Status", GetType(String))
        dtb.Columns.Add("Utility Status", GetType(String))
        dtb.Columns.Add("Turn On Date", GetType(String))
        dtb.Columns.Add("Aging", GetType(String))
        dtb.Columns.Add("System", GetType(String))
        dtb.Columns.Add("Vendor Info", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("Category", GetType(String))
        dtb.Columns.Add("Out of REO", GetType(String))
        dtb.Columns.Add("Electricity Status", GetType(String))
        dtb.Columns.Add("Electricity Turn on Date", GetType(String))
        dtb.Columns.Add("Gas Status", GetType(String))
        dtb.Columns.Add("Gas Turn On Date", GetType(String))
        dtb.Columns.Add("Exception Category", GetType(String))
        dtb.Columns.Add("Previous Status of Exception", GetType(String))
        dtb.Columns.Add("Actual Aging", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [Event Completion(MTD)-Prev Day$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[Event Completion(MTD)-Prev Day$]")

                For i As Integer = 1 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_EventCompletionPrevDay", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_EventCompletionPrevDay where [Property ID] = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportPFCActivation()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("STATE-SHORT NAME", GetType(String))
        dtb.Columns.Add("PROPERTY ID", GetType(String))
        dtb.Columns.Add("WORK ITEM", GetType(String))
        dtb.Columns.Add("WI CLOSED DATE", GetType(String))
        dtb.Columns.Add("AGING", GetType(String))
        dtb.Columns.Add("CATEGORY", GetType(String))
        dtb.Columns.Add("WORK TASK", GetType(String))
        dtb.Columns.Add("OCCUPANCY STATUS", GetType(String))
        dtb.Columns.Add("IPR REVIEW DATE", GetType(String))
        dtb.Columns.Add("CLIENT STATUS", GetType(String))
        dtb.Columns.Add("PORTFOLIO NAME", GetType(String))
        dtb.Columns.Add("UTILITY COORDINATOR ID", GetType(String))
        dtb.Columns.Add("NAME", GetType(String))
        dtb.Columns.Add("DOES THE PROPERTY HAVE A SUMP PUMP?", GetType(String))
        dtb.Columns.Add("GAS STATUS", GetType(String))
        dtb.Columns.Add("GAS TURN ON DATE", GetType(String))
        dtb.Columns.Add("GAS CONFIRMED ACTIVATION", GetType(String))
        dtb.Columns.Add("GAS STATUS DATE", GetType(String))
        dtb.Columns.Add("ELEC STATUS", GetType(String))
        dtb.Columns.Add("ELEC TURN ON DATE", GetType(String))
        dtb.Columns.Add("ELEC CONFIRMED ACTIVATION", GetType(String))
        dtb.Columns.Add("ELEC STATUS DATE", GetType(String))
        dtb.Columns.Add("WATER STATUS", GetType(String))
        dtb.Columns.Add("WATER TURN ON DATE", GetType(String))
        dtb.Columns.Add("WATER CONFIRMED ACTIVATION", GetType(String))
        dtb.Columns.Add("WATER STATUS DATE", GetType(String))
        dtb.Columns.Add("CLIENT", GetType(String))

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [PFC Sump Pump$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[PFC Sump Pump$]")

                For i As Integer = 1 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(22).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(23).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(24).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(25).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(26).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_PFCSumpPump", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_PFCSumpPump where [PROPERTY ID] = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportRESIDeactivation()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("PROP_ID", GetType(String))
        dtb.Columns.Add("Out of REO Date", GetType(String))
        dtb.Columns.Add("REOStatus", GetType(String))
        dtb.Columns.Add("Username", GetType(String))
        dtb.Columns.Add("ElectricityStatus", GetType(String))
        dtb.Columns.Add("Electricity_Status_Changed_Date", GetType(String))
        dtb.Columns.Add("WaterStatus", GetType(String))
        dtb.Columns.Add("Water_Status_Changed_Date", GetType(String))
        dtb.Columns.Add("Gas_staus", GetType(String))
        dtb.Columns.Add("Gas_Status_Changed_Date", GetType(String))
        dtb.Columns.Add("Status", GetType(String))
        dtb.Columns.Add("E", GetType(String))
        dtb.Columns.Add("W", GetType(String))
        dtb.Columns.Add("G", GetType(String))
        dtb.Columns.Add("Total", GetType(String))
        dtb.Columns.Add("Completion Date", GetType(String))
        dtb.Columns.Add("Completion month", GetType(String))
        dtb.Columns.Add(".", GetType(String))
        dtb.Columns.Add("Actual Aging", GetType(String))
        dtb.Columns.Add("Aging Category", GetType(String))
        dtb.Columns.Add("SLA", GetType(String))
        dtb.Columns.Add("Addition Date", GetType(String))
        dtb.Columns.Add("New Addition", GetType(String))


        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [RESI Deactivation$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[RESI Deactivation$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_RESIDeactivation", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_RESIDeactivation where PROP_ID = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub ImportRESIActivation()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As New DataTable

        'dtb.Columns.Add("id", GetType(Integer))
        dtb.Columns.Add("PropertyID", GetType(String))
        dtb.Columns.Add("State", GetType(String))
        dtb.Columns.Add("[User Name]", GetType(String))
        dtb.Columns.Add("[Boarded Date]", GetType(String))
        dtb.Columns.Add("[Client Status]", GetType(String))
        dtb.Columns.Add("[WCONFIRM Date]", GetType(String))
        dtb.Columns.Add("[Electricity Status]", GetType(String))
        dtb.Columns.Add("[Electricity Service Provider Name]", GetType(String))
        dtb.Columns.Add("[Electricity Date]", GetType(String))
        dtb.Columns.Add("[Gas Status]", GetType(String))
        dtb.Columns.Add("[Gas Service Provider Name]", GetType(String))
        dtb.Columns.Add("[Gas Date]", GetType(String))
        dtb.Columns.Add("[Water Status]", GetType(String))
        dtb.Columns.Add("[Water Service Provider Name]", GetType(String))
        dtb.Columns.Add("[Water Date]", GetType(String))
        dtb.Columns.Add("[Out of REO Dates]", GetType(String))
        dtb.Columns.Add("[Property Status]", GetType(String))
        dtb.Columns.Add("[Follow Up Date/ ETA Date]", GetType(String))
        dtb.Columns.Add("Electricity", GetType(String))
        dtb.Columns.Add("Gas", GetType(String))
        dtb.Columns.Add("Water", GetType(String))
        dtb.Columns.Add("[Occupancy Status]", GetType(String))
        dtb.Columns.Add("Qualifier", GetType(String))
        dtb.Columns.Add("[Property Utility Status]", GetType(String))
        dtb.Columns.Add("Aging", GetType(String))
        dtb.Columns.Add("[Date Stamp/ As of Date]", GetType(String))
        dtb.Columns.Add("[Open Utility Count]", GetType(String))
        dtb.Columns.Add("Inflow", GetType(String))
        dtb.Columns.Add("SLA", GetType(String))


        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx" & ";Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select * From [RESI Activation$]", conn)
            dts = New DataSet

            Try
                dta.Fill(dts, "[RESI Activation$]")

                For i As Integer = 0 To dts.Tables(0).Rows.Count - 1
                    dtb.Rows.Add(dts.Tables(0).Rows(i)(0).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(1).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(2).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(3).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(4).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(5).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(6).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(7).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(8).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(9).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(10).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(11).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(12).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(13).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(14).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(15).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(16).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(17).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(18).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(19).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(20).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(21).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(22).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(23).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(24).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(25).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(26).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(27).ToString.Trim, _
                                 dts.Tables(0).Rows(i)(28).ToString.Trim)
                Next

                Try
                    cls.SQLBulkCopy("tbl_UC_CtrlRpt_RESIActivation", dtb)
                    cls.ExecuteQuery("delete from tbl_UC_CtrlRpt_RESIActivation where PropertyID = ''")
                Catch ex As Exception

                    errCount += 1
                    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
                End Try
            Catch ex As Exception

                errCount += 1
                'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
            End Try
        Catch ex As Exception

            errCount += 1
            'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

    End Sub

    Private Sub UpdateLog(ByVal val As String)

        Dim cls As New clsConnection
        Dim uqry As String = ""

        uqry += "update tbl_UC_ParameterReference set refvalue = '" & val & "' where refkey = 'LastCtrlRptModDate'" & vbCrLf
        uqry += "update tbl_UC_ParameterReference set refvalue = '" & Now().Date & "' where refkey = 'LastCtrlRptImportDate'" & vbCrLf
        uqry += "update tbl_UC_ParameterReference set refvalue = '1' where refkey = 'Update_CtrlRpt'"

        Try
            cls.ExecuteQuery(uqry)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetParameters()

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY in ('CTRLRPT_PATH', 'Update_CtrlRpt')")

            If dt.Rows.Count > 0 Then
                ctrlrptPath = dt.Rows(0)(0)
                ctrlrptstat = dt.Rows(1)(0)
            Else
                ctrlrptPath = ""
                ctrlrptstat = ""
            End If
        Catch ex As Exception
            ctrlrptPath = ""
            ctrlrptstat = ""
        End Try
    End Sub

    Sub Main()
        Dim CtrlRptFolder As String = ""
        Dim CtrlFile As String = ""
        Dim UtilityDataFile As String = ""
        Dim nDate As Date = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Date

        GetParameters()

        CtrlRptFilename = Format(nDate, "MMddyyyy") 'IIf(Weekday(nDate) = "2", Format(DateAdd(DateInterval.Day, -2, nDate.Date), "MMddyyyy"), Format(nDate.Date, "MMddyyyy")) 
        CtrlRptFolder = Format(nDate, "MMM") & "-" & Format(nDate, "yy") 'Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -2, nDate.Date), nDate.Date), "MMM") & "-" & Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -2, nDate.Date), nDate.Date), "yy") 
        CtrlFile = ctrlrptPath & CtrlRptFolder & "\MNL Utility Control Report " & CtrlRptFilename & ".xlsx"
        UtilityDataFile = "\\ascorp.com\data\Bangalore\CommonShare\Strategic$\internal\Operations\PPI-MIS\Internal\Reports - REO\Utility Reports\" & nDate.Year & "\Utilities Raw Data-" & Format(System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Date, "MMddyyyy") & ".xlsb"


        If File.Exists(CtrlFile) Then

            If System.IO.File.GetLastWriteTime(CtrlFile).Date.ToString("MM/dd/yyyy") <> GetLastCtrlRptModDate().ToString("MM/dd/yyyy") Then

                If DateDiff(DateInterval.Hour, GetLastCtrlRptModDate(), System.IO.File.GetLastWriteTime(CtrlFile)) > 0 And _
                   DateDiff(DateInterval.Hour, GetLastCtrlRptModDate(), System.IO.File.GetLastWriteTime(CtrlFile)) < 8 Then
                    Dim clsE As New clsEmail
                    clsE.CreateSendEmail("New control report file was found...", "Control Report Update")
                    Exit Sub
                End If

                Try

                    Console.WriteLine("Copying files to your local drive...")
                    Console.WriteLine("")
                    My.Computer.FileSystem.CopyFile(CtrlFile, IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utility Control Report " & CtrlRptFilename & ".xlsx", True)

                    CleanCtrlReport()

                    Console.WriteLine("Importing data for Deactivation...")
                    ImportDeactivation()
                    Console.WriteLine("Import done for Deactivation...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from Water Open Inventory...")
                    ImportWaterPayout()
                    Console.WriteLine("Import done for Water Payout...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from Water Payout...")
                    ImportWaterPayoutUndercontract()
                    Console.WriteLine("Import done for Water Payout Undercontract inventory...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from Open Inventory - Current Day...")
                    ImportREOActivation()
                    Console.WriteLine("Import done for REO Activation...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from Event Completion(MTD)-Cur Day...")
                    ImportREOActivationCurDay()
                    Console.WriteLine("Import done for Event Completion(MTD)-Cur Day...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from Event Completion(MTD)-Prev Day...")
                    ImportREOActivationPrevDay()
                    Console.WriteLine("Import done for Event Completion(MTD)-Prev Day...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from PFC Sump Pump...")
                    ImportPFCActivation()
                    Console.WriteLine("Import done for for PFC Activation...")
                    Console.WriteLine("")

                    Console.WriteLine("Importing data from RESI Activation...")
                    ImportRESIActivation()
                    Console.WriteLine("Import done for for RESI Activation...")
                    Console.WriteLine("")

                    'Console.WriteLine("Importing data from RESI-Open Inventory...")
                    'ImportRESIActivation()
                    'Console.WriteLine("Import done for for RESI Activation...")
                    'Console.WriteLine("")

                    'Console.WriteLine("Importing data from RESI Water Inventory...")
                    'ImportRESIWaterInv()
                    'Console.WriteLine("Import done for for RESI Water Inventory...")
                    'Console.WriteLine("")

                    'Console.WriteLine("Importing data from RESI Water Completion...")
                    'ImportRESIWaterCompl()
                    'Console.WriteLine("Import done for for RESI Water Completion...")
                    'Console.WriteLine("")

                    Console.WriteLine("Importing data from RESI Deactivation...")
                    ImportRESIDeactivation()
                    Console.WriteLine("Import done for for RESI Deactivation...")
                    Console.WriteLine("")

                    If errCount = 0 Then
                        UpdateLog(System.IO.File.GetLastWriteTime(CtrlFile).ToString)

                        Dim clsE As New clsEmail

                        clsE.CreateSendEmail("Control Report has been updated...", "Control Report Update")
                    End If
                Catch ex As Exception

                End Try
            End If
        End If

        'If File.Exists(UtilityDataFile) Then

        '    If System.IO.File.GetLastWriteTime(UtilityDataFile).Date > GetLastUtilityDataModDate() Then

        '        Console.WriteLine("Copying files to your local drive...")
        '        Console.WriteLine("")
        '        My.Computer.FileSystem.CopyFile(UtilityDataFile, IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Utilities Raw Data-" & Now().ToString("MMddyyyy") & ".xlsb", True)

        '        Dim cls As New clsConnection
        '        cls.ExecuteQuery("delete from tbl_UC_Undercontract")

        '        Console.WriteLine("Importing data for Undercontract...")
        '        ImportREO_W()
        '        ImportRESI_W()
        '        Console.WriteLine("Import done for Undercontract...")
        '        Console.WriteLine("")

        '        Dim clsu As New clsConnection
        '        Dim uqry As String = "update tbl_UC_ParameterReference set refvalue = '" & System.IO.File.GetLastWriteTime(UtilityDataFile) & "' where REFKEY = 'UtilityDataFileDate'"

        '        cls.ExecuteQuery(uqry)

        '        Dim clsE As New clsEmail

        '        clsE.CreateSendEmail("Undercontract report has been updated...", "Undercontract Report")
        '    End If
        'End If
    End Sub

End Module
