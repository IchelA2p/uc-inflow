﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class clsConnection

    Dim ConString As String = "Server=PIV8DBMISNP01;Initial Catalog=MIS_ALTI;Persist Security Info=True;User ID=mis414;Password=juchUt4a"
    
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim da As SqlDataAdapter

    Private _command As New SqlCommand

    Public Function GetData(ByVal Query As String) As DataTable

        Dim dt As New DataTable
        Try
            con = New SqlConnection(ConString)
            con.Open()
            da = New SqlDataAdapter(Query, con)

            da.Fill(dt)
            Return dt
        Catch ex As Exception
            Return dt
        End Try
    End Function

    Public Function ExecuteQuery(ByVal query As String) As Integer

        Dim i As Integer

        'Try
        con = New SqlConnection(ConString)
        con.Open()
        _command.Connection = con
        _command.CommandType = CommandType.Text
        _command.CommandText = query
        _command.CommandTimeout = 180

        i = _command.ExecuteNonQuery()
        _command.Dispose()
        'Catch ex As Exception
        'i = 0
        'End Try

        Return i
    End Function

    Public Sub SQLBulkCopy(ByVal table As String, ByVal dtb As DataTable)

        con = New SqlConnection(ConString)
        con.Open()

        Using copy As New SqlBulkCopy(con)
            copy.DestinationTableName = table
            copy.BulkCopyTimeout = 120
            copy.WriteToServer(dtb)
        End Using
    End Sub
End Class
