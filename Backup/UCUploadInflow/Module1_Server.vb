﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Net.Mime.MediaTypeNames
Imports Microsoft.Office.Interop
Imports System.Timers

Module Module1_Server

    Dim orldt As DateTime
    Dim ctrlrptPath As String = ""
    Dim trg As String = ""
    Dim interval As Integer = 15
    Dim fdt As DataTable
    Dim cnt As Integer = 0
    Dim CtrlRptFilename As String = ""
    Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
    Dim clsMail As New clsEmail
    Public deact, wp, act_REO, act_PFC, act_RESI, deact_RESI As Integer
    Dim ObjArra(3) As Object

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Function GetNextRunInterval() As String

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UPLOADINFLOW_INTERVAL'")

            If dt.Rows.Count > 0 Then
                interval = dt.Rows(0)(0)
                Return dt.Rows(0)(0)
            Else
                interval = 15
                Return "15"
            End If
        Catch ex As Exception
            interval = 15
            Return "15"
        End Try
    End Function

    Private Sub GetParameters()

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY in ('LASTMODDATE_OPENRPT','CTRLRPT_PATH','Update_CtrlRpt')")

            If dt.Rows.Count > 0 Then
                orldt = dt.Rows(0)(0)
                ctrlrptPath = dt.Rows(1)(0)
                trg = dt.Rows(2)(0)
            Else
                orldt = ""
                ctrlrptPath = ""
                trg = ""
            End If
        Catch ex As Exception
            orldt = ""
            trg = ""
        End Try
    End Sub

    Private Sub UpdateORLastDate(ByVal val As String)

        Dim cls As New clsConnection

        Try
            cls.ExecuteQuery("update tbl_uc_ParameterReference set refvalue = '" & val & "' where refkey = 'LASTMODDATE_OPENRPT'" & vbCrLf & _
                             "update tbl_UC_ParameterReference set REFVALUE = '1' where REFKEY = 'Update_OpenRpt'")
        Catch ex As Exception

        End Try
    End Sub

    Sub GetOpenReport_OLSR()

        Dim cls As New clsConnection
        Dim conn As New OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #], DateUploaded From [OLSR$]", conn)
            dta = New OleDbDataAdapter("Select  property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], " & _
                                       "[Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Gas Work Item], " & _
                                       "[Gas WI Status], [Gas WI Issued Date], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Elec Work Item], [Elec WI Status], [Elec WI Issued Date], " & _
                                       "[Water Vendor ID], [Water Last Update], [Water Work Item #], [Water Work Item], [Water WI Status], [Water WI Issued Date], Zip_Code, [Gas Meter #], " & _
                                       "[Elec Meter #], [Water Meter #], [Foreclosure Date], [Lock Box Code] From [OLSR$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsb for OLSR." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try


        Try
            dta.Fill(dts, "[OLSR$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsb for OLSR." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsb for OLSR to the database." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery("update tbl_UC_OpenInventory set ClientCode = 'OLSR', DateUploaded = '" & Now() & "' where ClientCode IS NULL") '("delete from tbl_UC_OpenInventory where property_code is null")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("OLSR Import finished...")
    End Sub

    Sub GetOpenReport_PFC()

        Dim cls As New clsConnection
        Dim conn As New OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try

            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #], DateUploaded From [PFC$]", conn)
            dta = New OleDbDataAdapter("Select  property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], " & _
                                       "[Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Gas Work Item], " & _
                                       "[Gas WI Status], [Gas WI Issued Date], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Elec Work Item], [Elec WI Status], [Elec WI Issued Date], " & _
                                       "[Water Vendor ID], [Water Last Update], [Water Work Item #], [Water Work Item], [Water WI Status], [Water WI Issued Date], Zip_Code, [Gas Meter #], " & _
                                       "[Elec Meter #], [Water Meter #], [Foreclosure Date], [Lock Box Code] From [PFC$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsb for PFC." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[PFC$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsb for PFC." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsb for PFC to the database." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery("update tbl_UC_OpenInventory set ClientCode = 'PFC', DateUploaded = '" & Now() & "' where ClientCode IS NULL")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("PFC Import finished...")
        'Console.ReadLine()
    End Sub

    Sub GetOpenReport_RESI()

        Dim cls As New clsConnection
        Dim conn As New OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #], DateUploaded From [RESI$]", conn)
            dta = New OleDbDataAdapter("Select  property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], " & _
                                       "[Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Gas Work Item], " & _
                                       "[Gas WI Status], [Gas WI Issued Date], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Elec Work Item], [Elec WI Status], [Elec WI Issued Date], " & _
                                       "[Water Vendor ID], [Water Last Update], [Water Work Item #], [Water Work Item], [Water WI Status], [Water WI Issued Date], Zip_Code, [Gas Meter #], " & _
                                       "[Elec Meter #], [Water Meter #], [Foreclosure Date], [Lock Box Code] From [RESI$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsb for RESI." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[RESI$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsb for RESI." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsb for RESI to the database." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery("update tbl_UC_OpenInventory set ClientCode = 'RESI', DateUploaded = '" & Now() & "' where ClientCode IS NULL")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("RESI Import finished...")
        'Console.ReadLine()
    End Sub

    Sub GetOpenReport_RESI_FAY()

        Dim cls As New clsConnection
        Dim conn As New OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #], DateUploaded From [RESI_FAY$]", conn)
            dta = New OleDbDataAdapter("Select  property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], " & _
                                       "[Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Gas Work Item], " & _
                                       "[Gas WI Status], [Gas WI Issued Date], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Elec Work Item], [Elec WI Status], [Elec WI Issued Date], " & _
                                       "[Water Vendor ID], [Water Last Update], [Water Work Item #], [Water Work Item], [Water WI Status], [Water WI Issued Date], Zip_Code, [Gas Meter #], " & _
                                       "[Elec Meter #], [Water Meter #], [Foreclosure Date], [Lock Box Code] From [RESI_FAY$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsb for RESI_FAY." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[RESI_FAY$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsb for RESI_FAY." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsb for RESI_FAY to the database." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery("update tbl_UC_OpenInventory set ClientCode = 'RESI_FAY', DateUploaded = '" & Now() & "' where ClientCode IS NULL")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("RESI_FAY Import finished...")
        'Console.ReadLine()
    End Sub

    Sub GetOpenReport_AllData(ByVal worksheet As String)

        Dim cls As New clsConnection
        Dim conn As New OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #], DateUploaded From [RESI_FAY$]", conn)
            dta = New OleDbDataAdapter("Select  [Property ID], [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], " & _
                                       "[Water Account #], [Water USP Contact], [Water USP], [Water Status], [Property Address], [Postal Code], [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Gas Work Item], " & _
                                       "[Gas WI Status], [Gas Order Date], [Gas WI Type], [Gas Status Reason], [Gas WI Status Date], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Elec Work Item], [Elec WI Status], [Elec Order Date], [Elec WI Type], [Elec Status Reason], [Elec WI Status Date], " & _
                                       "[Water Vendor ID], [Water Last Update], [Water Work Item #], [Water Work Item], [Water WI Status], [Water Order Date], [Water WI Type], [Water Status Reason], [Water WI Status Date], [Zip Code], [Gas Meter #], " & _
                                       "[Elec Meter #], [Water Meter #], [Foreclosure Date], [Lock Box Code], [# of Units], [Primary Vendor ID], [Primary Vendor], [Primary VDR Contact], " & _
                                       "[Primary VDR Business Phone], [Primary VDR Mobile], [Primary VDR 24HContact], [Borrower], [Property Status] From [" & worksheet & "$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsb for " & worksheet & "." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[" & worksheet & "$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsb for " & worksheet & "." & vbCrLf & ex.ToString)
            conn.Close()
            Exit Sub
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsb for " & worksheet & " to the database." & vbCrLf & ex.ToString)
            Exit Sub
        End Try
    End Sub

    Private Function FormatCells() As Boolean

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As New Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim lastrow As Integer
        xlApp = New Excel.ApplicationClass

        Try
            xlWorkBook = xlApp.Workbooks.Open(IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb", misValue, misValue)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "Unable to access UC Open Report.xlsb." & vbCrLf & ex.ToString)
            Return False
            Exit Function
        End Try

        'xlWorkBook = xlApp.Workbooks.Open("C:\Users\elayronj\Documents\UC Open Report.xlsb")

        Try
            For i = 1 To xlWorkBook.Sheets.Count

                'If i = 1 Then
                '    xlWorkSheet = xlWorkBook.Worksheets(1)
                'ElseIf i = 2 Then
                '    xlWorkSheet = xlWorkBook.Worksheets(2)
                'ElseIf i = 3 Then
                '    xlWorkSheet = xlWorkBook.Worksheets(3)
                '    'Else
                '    '    xlWorkSheet = xlWorkBook.Worksheets("RESI_FAY")
                'End If

                xlWorkSheet = xlWorkBook.Worksheets(i)
                ObjArra(i - 1) = xlWorkSheet.Name

                lastrow = xlWorkSheet.Rows.End(Excel.XlDirection.xlDown).Row

                xlWorkSheet.Range("k1", "k" & lastrow.ToString).NumberFormat = "######################" 'Gas Account #
                xlWorkSheet.Range("y1", "y" & lastrow.ToString).NumberFormat = "######################" 'Elec Account #
                xlWorkSheet.Range("am1", "am" & lastrow.ToString).NumberFormat = "######################" 'Water Account #

                xlWorkSheet.Range("l1", "l" & lastrow.ToString).NumberFormat = "################" 'Gas USP Contact
                xlWorkSheet.Range("z1", "z" & lastrow.ToString).NumberFormat = "################" 'Elec USP Contact
                xlWorkSheet.Range("an1", "an" & lastrow.ToString).NumberFormat = "################" 'Water USP Contact

                xlWorkSheet.Range("i1", "i" & lastrow.ToString).NumberFormat = "################" 'Gas Vendor ID
                xlWorkSheet.Range("w1", "w" & lastrow.ToString).NumberFormat = "################" 'Elec Vendor ID
                xlWorkSheet.Range("ak1", "ak" & lastrow.ToString).NumberFormat = "################" 'Water Vendor ID

                xlWorkSheet.Range("j1", "j" & lastrow.ToString).NumberFormat = "######################" 'Gas Meter #
                xlWorkSheet.Range("x1", "x" & lastrow.ToString).NumberFormat = "######################" 'Elec Meter #
                xlWorkSheet.Range("al1", "al" & lastrow.ToString).NumberFormat = "######################" 'Water Meter #

                xlWorkSheet.Range("n1", "n" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Gas Last Update
                xlWorkSheet.Range("p1", "p" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Gas Order Date
                xlWorkSheet.Range("t1", "t" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Gas WI Status Date

                xlWorkSheet.Range("ab1", "ab" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Elec Last Update
                xlWorkSheet.Range("ad1", "ad" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Elec Order Date
                xlWorkSheet.Range("ah1", "ah" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Elec WI Status Date

                xlWorkSheet.Range("ap1", "ap" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Water Last Update
                xlWorkSheet.Range("ar1", "ar" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Water Order Date
                xlWorkSheet.Range("av1", "av" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Water WI Status Date

                'xlWorkSheet.Range("as1", "as" & lastrow.ToString).Replace("NULL", "1900/01/01", 2, 1, misValue, misValue, misValue, misValue) 'Foreclosure Date

                xlWorkSheet.Range("az1", "az" & lastrow.ToString).NumberFormat = "@"
                xlWorkSheet.Range("bb1", "bb" & lastrow.ToString).NumberFormat = "@"
                xlWorkSheet.Range("bf1", "bf" & lastrow.ToString).NumberFormat = "@"
                xlWorkSheet.Range("bg1", "bg" & lastrow.ToString).NumberFormat = "@"

                xlWorkSheet.Range("bg1").Value = "Primary VDR 24HContact"
            Next
        Catch ex As Exception
            xlWorkBook.Close(True, "UC Open Report", misValue)
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while formatting UC Open Report.xlsb. A certain data could not be formatted or the fields of the file might have changed." & vbCrLf & ex.ToString)
            Return False
            Exit Function
        End Try

        Try
            xlWorkBook.Save()

            xlWorkBook.Close(True, "UC Open Report", misValue)
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            Return True
        Catch ex As Exception

            xlWorkBook.Close(True, "UC Open Report", misValue)
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while saving the UC Open Report.xlsb." & vbCrLf & ex.ToString)

            Return False
        End Try

        'Try
        '    xlWorkBook.Close(True, "UC Open Report", misValue)
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'Catch ex As Exception
        '    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while closing the UC Open Report.xlsb." & vbCrLf & ex.ToString)
        '    Return False
        'End Try
    End Function

    Sub GetCountInflow()

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select COUNT(*), 1 Task from dbo.fn_UC_Inflow(1) union select COUNT(*), 2 Task from dbo.fn_UC_Inflow(2) union select COUNT(*), 3 Task from dbo.fn_UC_Inflow(3) union select COUNT(*), 4 Task from dbo.fn_UC_Inflow(4) union select COUNT(*), 5 Task from dbo.fn_UC_Inflow(5) union select COUNT(*), 7 Task from dbo.fn_UC_Inflow(7) order by Task")

            deact = dt.Rows(0).Item(0)
            wp = CInt(dt.Rows(1).Item(0))
            act_REO = dt.Rows(2).Item(0)
            act_PFC = dt.Rows(3).Item(0)
            act_RESI = dt.Rows(4).Item(0)
            deact_RESI = dt.Rows(5).Item(0)
        Catch ex As Exception
            deact = 0
            wp = 0
            act_REO = 0
            act_PFC = 0
            act_RESI = 0
            deact_RESI = 0
        End Try
    End Sub

    Sub UploadInflow()

        Dim cls As New clsConnection
        Dim iqry As String = ""

        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(1)" & vbCrLf
        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(2)" & vbCrLf


        iqry += "insert into tbl_UC_ActivationDetails (PropertyCode, UtilityType, Category, [Status], PostedBy, PostedDate)" & vbCrLf
        iqry += "select distinct tbl_OutofREO.[Property ID], tbl_OutofREO.Utility, 'REO', x.[Status], x.PostedBy, x.PostedDate from" & vbCrLf
        iqry += "(" & vbCrLf
        iqry += "select a.[Property ID], a.[Electricity Status] [UtilityStatus], 'Elec' [Utility], a.[Electricity Turn on Date] [TurnONDate]" & vbCrLf
        iqry += "from dbo.tbl_UC_CtrlRpt_EventCompletionPrevDay a" & vbCrLf
        iqry += "inner join dbo.tbl_UC_CtrlRpt_EventCompletionCurDay b" & vbCrLf
        iqry += "on a.[Property ID] = b.[Asset wise]" & vbCrLf
        iqry += "where a.[Electricity Status] = 'Out of REO'" & vbCrLf
        iqry += "union" & vbCrLf
        iqry += "select a.[Property ID], a.[Gas Status] [UtilityStatus], 'Gas' [Utility], a.[Gas Turn On Date]" & vbCrLf
        iqry += "from dbo.tbl_UC_CtrlRpt_EventCompletionPrevDay a" & vbCrLf
        iqry += "inner join dbo.tbl_UC_CtrlRpt_EventCompletionCurDay b" & vbCrLf
        iqry += "on a.[Property ID] = b.[Asset wise]" & vbCrLf
        iqry += "where a.[Gas Status] = 'Out of REO'" & vbCrLf
        iqry += ") as tbl_OutofREO" & vbCrLf
        iqry += "inner join tbl_UC_Activation x" & vbCrLf
        iqry += "on tbl_OutofREO.[Property ID] = x.PropertyCode and tbl_OutofREO.Utility = x.Utility" & vbCrLf
        iqry += "order by x.PostedDate" & vbCrLf


        iqry += "update x set x.PreviousStatus = x.[Status], x.[Status] = 'Out of REO', x.PostedBy = 'Auto', x.PostedDate = GETDATE(), Vmstagged = 1, Vmsposteddate = getdate(), Taggedas = tbl_OutofREO.[UtilityStatus], x.UpdateFlag = 1" & vbCrLf
        iqry += "from" & vbCrLf
        iqry += "(" & vbCrLf
        iqry += "select a.[Property ID], a.[Electricity Status] [UtilityStatus], 'Elec' [Utility], a.[Electricity Turn on Date] [TurnONDate]" & vbCrLf
        iqry += "from dbo.tbl_UC_CtrlRpt_EventCompletionPrevDay a" & vbCrLf
        iqry += "inner join dbo.tbl_UC_CtrlRpt_EventCompletionCurDay b" & vbCrLf
        iqry += "on a.[Property ID] = b.[Asset wise]" & vbCrLf
        iqry += "where a.[Electricity Status] = 'Out of REO'" & vbCrLf
        iqry += "union" & vbCrLf
        iqry += "select a.[Property ID], a.[Gas Status] [UtilityStatus], 'Gas' [Utility], a.[Gas Turn On Date]" & vbCrLf
        iqry += "from dbo.tbl_UC_CtrlRpt_EventCompletionPrevDay a" & vbCrLf
        iqry += "inner join dbo.tbl_UC_CtrlRpt_EventCompletionCurDay b" & vbCrLf
        iqry += "on a.[Property ID] = b.[Asset wise]" & vbCrLf
        iqry += "where a.[Gas Status] = 'Out of REO'" & vbCrLf
        iqry += ") as tbl_OutofREO" & vbCrLf
        iqry += "inner join tbl_UC_Activation x" & vbCrLf
        iqry += "on tbl_OutofREO.[Property ID] = x.PropertyCode and tbl_OutofREO.Utility = x.Utility" & vbCrLf


        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(3)" & vbCrLf
        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(4)" & vbCrLf
        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(5)" & vbCrLf
        iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(7)" & vbCrLf
        'iqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo, LockCode, ForeclosureDate, BorrowersName, Origin) select * from dbo.fn_UC_Inflow(8)" & vbCrLf

        Try
            cls.ExecuteQuery(iqry)

            GetCountInflow()
        Catch ex As Exception
            clsMail.CreateSendEmail(1, deact, wp, act_REO, act_PFC, act_RESI, deact_RESI, "An error occurred while trying to execute the query " & vbCrLf & vbCrLf & iqry & vbCrLf & vbCrLf & ex.ToString)
        End Try
    End Sub

    Sub Main()

        deact = 0
        wp = 0
        act_REO = 0
        act_PFC = 0
        act_RESI = 0

        GetParameters()

        Dim CtrlRptFolder As String = ""
        Dim nDate As Date = Now().Date

        CtrlRptFilename = Format(nDate.Date, "MMddyyyy") 'IIf(Weekday(nDate) = "2", Format(DateAdd(DateInterval.Day, -3, nDate.Date), "MMddyyyy"), Format(DateAdd(DateInterval.Day, -1, nDate.Date), "MMddyyyy"))

        CtrlRptFolder = Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -3, nDate.Date), DateAdd(DateInterval.Day, -1, nDate.Date)), "MMM") & "-" & Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -3, nDate.Date), DateAdd(DateInterval.Day, -1, nDate.Date)), "yy")

        Dim CtrlFile As String = ctrlrptPath & CtrlRptFolder & "\Utility Control Report " & CtrlRptFilename & ".xlsx"
        Dim OpenRptFile As String = "\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Manila Schedule\UC Open Report.xlsb"
        Dim cls As New clsConnection

        If File.Exists(OpenRptFile) Then

            If orldt <> System.IO.File.GetLastWriteTime(OpenRptFile).ToString And trg = "1" Then

                'If orldt = System.IO.File.GetLastWriteTime(OpenRptFile).ToString Then

                '    Console.WriteLine("UC Open Report's Last Modified Date is " & System.IO.File.GetLastWriteTime(OpenRptFile) & "")

                '    'clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "UC Open Report's Last Modified Date is " & System.IO.File.GetLastWriteTime(OpenRptFile).ToShortDateString().ToString() & "")
                'Else

                If DateDiff(DateInterval.Hour, orldt, System.IO.File.GetLastWriteTime(OpenRptFile)) > 0 And _
                   DateDiff(DateInterval.Hour, orldt, System.IO.File.GetLastWriteTime(OpenRptFile)) < 8 Then

                    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "New open report file was found...")
                    Exit Sub
                End If

                Console.WriteLine("UC Open Report File found...")

                Try
                    Console.WriteLine("Copying the file to your local drive...")

                    My.Computer.FileSystem.CopyFile(OpenRptFile, IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\UC Open Report.xlsb", True)
                Catch ex As Exception
                    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "Could not copy the file UC Open Report.xlsb to drive D." & ex.ToString)
                    Exit Sub
                End Try

                Console.WriteLine("Formatting cells...")

                If FormatCells() = False Then
                    Exit Sub
                End If

                Try
                    cls.ExecuteQuery("delete from tbl_UC_OpenInventory")
                Catch ex As Exception
                    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory}" & vbCrLf & ex.ToString)
                    Exit Sub
                End Try

                For i = 0 To ObjArra.Length - 2

                    Console.WriteLine("Uploading data for " & ObjArra(i) & "...")
                    GetOpenReport_AllData(ObjArra(i))
                    Console.WriteLine("Done...")
                Next

                Try
                    cls.ExecuteQuery("delete from tbl_UC_OpenInventory where property_code is null")

                    cls.ExecuteQuery("update tbl_UC_OpenInventory set DateUploaded = '" & Now() & "'")
                Catch ex As Exception
                    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null | update tbl_UC_OpenInventory set DateUploaded = '" & Now() & "'}" & vbCrLf & ex.ToString)
                    Exit Sub
                End Try

                Console.WriteLine("Uploading Inflow. This may take several minutes...")
                UploadInflow()

                UpdateORLastDate(System.IO.File.GetLastWriteTime(OpenRptFile))

                clsMail.CreateSendEmail(1, deact, wp, act_REO, act_PFC, act_RESI, deact_RESI, "Upload Inflow successful.")

            End If
            'Else
            '    Console.WriteLine("Unable to connect to the Server...")

            '    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, "System was unable to connect to the server. Please contact your local system administrator.")
            'End If
        Else

            Console.WriteLine("UC Open Report File not found...")

            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, 0, OpenRptFile & " was not found.")
        End If

    End Sub
End Module
