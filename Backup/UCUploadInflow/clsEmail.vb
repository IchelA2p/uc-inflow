﻿Imports System.Web.Mail
'Imports System.Net.Mail

Public Class clsEmail

    Dim sto As String
    Dim cc As String

    Private Sub GetRecipients()

        Dim cls As New clsConnection
        Dim dt As New DataTable

        Try
            dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY IN ('UCINFLOW_SENTTO','UCINFLOW_CC')")

            If dt.Rows.Count > 0 Then
                sto = dt.Rows(0)(0)
                'cc = dt.Rows(1)(0)
            Else
                sto = ""
                cc = ""
            End If
        Catch ex As Exception
            sto = ""
            cc = ""
        End Try
    End Sub

    Public Sub CreateSendEmail(ByVal mode As Integer, ByVal deact As Integer, ByVal wp As Integer, ByVal act_REO As Integer, ByVal act_PFC As Integer, ByVal act_RESI As Integer, ByVal deact_RESI As Integer, ByVal err As String)

        Dim e_frm, e_body, e_server, e_subj As String
        Dim oMsg As MailMessage = New MailMessage()
        'Dim dt As DataTable = MailDtls()
        Dim val As Integer = 0

        GetRecipients()

        e_frm = "DEV_MNL@altisource.com"
        e_subj = "Upload Inflow " & Now().Date

        e_body = ""
        e_body += "Deactivation (REO): " & deact & vbCrLf
        e_body += "Water Payout (REO): " & wp & vbCrLf
        e_body += "Activation (REO): " & act_REO & vbCrLf
        e_body += "Activation (PFC): " & act_PFC & vbCrLf
        e_body += "Activation (RESI): " & act_RESI & vbCrLf
        e_body += "Deactivation (RESI): " & deact_RESI & vbCrLf & vbCrLf

        e_body += "Remarks: " & vbCrLf
        e_body += err

        e_server = "Internal-Mail.ascorp.com"


        ' TODO: Replace with sender e-mail address.
        oMsg.From = e_frm
        ' TODO: Replace with recipient e-mail address.
        oMsg.To = sto
        'oMsg.Cc = cc
        oMsg.Subject = e_subj

        ' SEND IN HTML FORMAT (comment this line to send plain text).
        oMsg.BodyFormat = MailFormat.Text

        ' ADD AN ATTACHMENT.
        ' TODO: Replace with path to attachment.
        'Dim sFile As String = "C:\Users\atuprich\Desktop\_hr\map.jpg"
        'Dim oAttch As MailAttachment = New MailAttachment(sFile, MailEncoding.Base64)

        'oMsg.Attachments.Add(oAttch)

        'HTML Body (remove HTML tags for plain text).

        oMsg.Body = e_body


        ' TODO: Replace with the name of your remote SMTP server.
        SmtpMail.SmtpServer = e_server

        Try
            SmtpMail.Send(oMsg)

            Console.WriteLine("Email successfully sent...")
        Catch ex As Exception
            Console.WriteLine("Email was not sent...")
        End Try

        oMsg = Nothing

        'oAttch = Nothing

        'Dim Outl As Object
        'Dim omsg As Object

        'Dim emailBody, emailSubj As String

        'emailSubj = "Upload Inflow " & Now().Date
        'emailBody = ""

        'emailBody += "Deactivation: " & deact & vbCrLf
        'emailBody += "Water Payout: " & wp & vbCrLf
        'emailBody += "Activation: " & activation & vbCrLf & vbCrLf

        'emailBody += "Remarks: " & vbCrLf
        'emailBody += err

        'Outl = CreateObject("Outlook.Application")

        'If Outl IsNot Nothing Then

        '    omsg = Outl.CreateItem(0)
        '    omsg.subject = emailSubj
        '    omsg.To = "MaCarmela.Coronel@altisource.com; Heherson.Blas@altisource.com; Richel.Atup@altisource.com"
        '    'omsg.To = "Richel.Atup@altisource.com"
        '    omsg.cc = "DEV_MNL@altisource.com"
        '    omsg.body = emailBody

        '    Try
        '        omsg.Send()

        '        Console.WriteLine("Email successfully sent...")
        '    Catch ex As Exception

        '        Console.WriteLine("Email was not sent...")
        '    End Try
        'End If
    End Sub
End Class
