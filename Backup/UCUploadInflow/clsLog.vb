﻿Imports System.IO

Public Class clsLog

    Public Sub CreateLog(ByVal Log As String)

        Dim strFile As String
        Dim sw As StreamWriter

        strFile = IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Upload Inflow Log " & Now().Month & ".txt"     'DateTime.Today.ToString("dd-MMM-yyyy")

        Try
            If (Not File.Exists(strFile)) Then
                sw = File.CreateText(strFile)
                'sw.WriteLine("Start Error Log for today")
            Else
                sw = File.AppendText(strFile)
                sw.WriteLine(Now() & " : ")
            End If

            sw.Close()
        Catch ex As IOException

            'MsgBox("Error writing to log file.")
            sw.Close()
        End Try
    End Sub
End Class
