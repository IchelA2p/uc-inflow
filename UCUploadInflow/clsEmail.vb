﻿'Imports System.Web.Mail
Imports System.Net.Mail

Public Class clsEmail

    Dim sto() As String
    Dim cc() As String

    Private Sub GetRecipients(ByVal stat As Integer)

        Dim cls As New clsConnection
        Dim dt As New DataTable
        Dim dtcc As New DataTable

        Try

            If stat = 0 Then 'SUCCESS

                dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UCINFLOW_SENTTO_DEV'")
            ElseIf stat = 1 Then 'VMS ERROR

                dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UCINFLOW_SENTTO_VMS'")
            ElseIf stat = 2 Then 'DEV ERROR

                dt = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UCINFLOW_SENTTO_DEV'")
            End If

            dtcc = cls.GetData("select refvalue from tbl_UC_ParameterReference where REFKEY = 'UCINFLOW_CC'")

            If dt.Rows.Count > 0 Then
                sto = dt.Rows(0)(0).ToString.Split(CChar(";"))
            Else
                sto = Nothing
            End If

            If dtcc.Rows.Count > 0 Then
                cc = dtcc.Rows(0)(0).ToString.Split(CChar(";"))
            Else
                cc = Nothing
            End If

        Catch ex As Exception
            sto = Nothing
            cc = Nothing
        End Try
    End Sub

    Public Sub CreateSendEmail(ByVal mode As Integer, ByVal deact As Integer, ByVal wp As Integer, ByVal act_REO As Integer, ByVal act_PFC As Integer, ByVal act_RESI As Integer, ByVal deact_RESI As Integer, ByVal err As String)

        Dim e_frm, e_body, e_server, e_subj As String
        Dim oMsg As MailMessage = New MailMessage()
        'Dim dt As DataTable = MailDtls()
        Dim val As Integer = 0

        GetRecipients(mode)

        e_frm = "DEV_MNL@altisource.com"
        e_subj = "Upload Inflow " & Now().Date & " - " & IIf(mode = 0, "SUCCESS", "FAIL")

        e_body = ""
        e_body += "Deactivation (REO): " & deact & vbCrLf
        e_body += "Water Payout (REO): " & wp & vbCrLf
        e_body += "Activation (REO): " & act_REO & vbCrLf
        e_body += "Activation (PFC): " & act_PFC & vbCrLf
        e_body += "Activation (RESI): " & act_RESI & vbCrLf
        e_body += "Deactivation (RESI): " & deact_RESI & vbCrLf & vbCrLf

        e_body += "Remarks: " & vbCrLf
        e_body += err

        e_server = "Internal-Mail.ascorp.com"


        ' TODO: Replace with sender e-mail address.
        oMsg.From = New MailAddress(e_frm)
        ' TODO: Replace with recipient e-mail address.

        For Each _to As String In sto
            oMsg.To.Add(New MailAddress(_to))
        Next

        For Each _cc As String In cc
            oMsg.CC.Add(New MailAddress(_cc))
        Next

        oMsg.Subject = e_subj

        ' SEND IN HTML FORMAT (comment this line to send plain text).
        'oMsg.BodyFormat = MailFormat.Text


        ' ADD AN ATTACHMENT.
        ' TODO: Replace with path to attachment.
        'Dim sFile As String = "C:\Users\atuprich\Desktop\_hr\map.jpg"
        'Dim oAttch As MailAttachment = New MailAttachment(sFile, MailEncoding.Base64)

        'oMsg.Attachments.Add(oAttch)

        'HTML Body (remove HTML tags for plain text).

        oMsg.Body = e_body


        ' TODO: Replace with the name of your remote SMTP server.



        'SmtpMail.SmtpServer = e_server

        Dim smtp As New Net.Mail.SmtpClient(e_server)
        smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
        smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

        Try

            smtp.Send(oMsg)
            'SmtpMail.Send(oMsg)

            Console.WriteLine("Email successfully sent...")
        Catch ex As Exception
            Console.WriteLine("Email was not sent...")
            Console.ReadLine()
        End Try

        oMsg = Nothing

        'oAttch = Nothing

        'Dim Outl As Object
        'Dim omsg As Object

        'Dim emailBody, emailSubj As String

        'emailSubj = "Upload Inflow " & Now().Date
        'emailBody = ""

        'emailBody += "Deactivation: " & deact & vbCrLf
        'emailBody += "Water Payout: " & wp & vbCrLf
        'emailBody += "Activation: " & activation & vbCrLf & vbCrLf

        'emailBody += "Remarks: " & vbCrLf
        'emailBody += err

        'Outl = CreateObject("Outlook.Application")

        'If Outl IsNot Nothing Then

        '    omsg = Outl.CreateItem(0)
        '    omsg.subject = emailSubj
        '    omsg.To = "MaCarmela.Coronel@altisource.com; Heherson.Blas@altisource.com; Richel.Atup@altisource.com"
        '    'omsg.To = "Richel.Atup@altisource.com"
        '    omsg.cc = "DEV_MNL@altisource.com"
        '    omsg.body = emailBody

        '    Try
        '        omsg.Send()

        '        Console.WriteLine("Email successfully sent...")
        '    Catch ex As Exception

        '        Console.WriteLine("Email was not sent...")
        '    End Try
        'End If
    End Sub
End Class
