﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Net.Mime.MediaTypeNames
Imports Microsoft.Office.Interop

Module Module1

    Dim fdt As DataTable
    Dim cnt As Integer = 0
    Dim CtrlRptFilename As String = ""
    Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
    Dim clsMail As New clsEmail
    Public deact, wp, act_REO, act_PFC, act_RESI As Integer

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub UpdateORLastDate(ByVal val As String)

        Dim cls As New clsConnection

        Try
            cls.ExecuteQuery("update tbl_uc_ParameterReference set refvalue = '" & val & "' where refkey = 'LASTMODDATE_OPENRPT'")
        Catch ex As Exception

        End Try
    End Sub

    Sub GetCtrlReport_Deact()

        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            dta = New OleDbDataAdapter("Select * From [Deactivation$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to Utility Control Report " & CtrlRptFilename & ".xlsx/Deactivation" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[Deactivation$]")
            dtb = dts.Tables(0)

            conn.Close()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from Utility Control Report " & CtrlRptFilename & ".xlsx/Deactivation" & vbCrLf & ex.ToString)
            Exit Sub
        End Try


        Console.WriteLine("UC Utility Ctrol Report: " & dts.Tables(0).Rows.Count.ToString & " records found for Deactivation...")
        'Console.ReadLine()

        Dim cls As New clsConnection
        Dim iqry As String = ""
        Dim finqry As String = ""

        Console.WriteLine("Creating the query..")
        'Console.ReadLine()

        finqry += "BEGIN TRY drop table  #DeactCtrlRpt END TRY BEGIN CATCH print 'ok' END CATCH " & vbCrLf

        finqry += "select * into #DeactCtrlRpt from (select '                    ' PROP_ID, '     ' Utility, '     ' Ageing) a" & vbCrLf & vbCrLf
        finqry += "delete from #DeactCtrlRpt " & vbCrLf & vbCrLf

        Console.WriteLine(finqry)
        'Console.ReadLine()

        Try
            For i As Integer = 0 To dtb.Rows.Count - 1

                iqry = ""

                If IIf(IsDBNull(dtb.Rows(i).Item("Electricity Status")), "", dtb.Rows(i).Item("Electricity Status")) = "Out of REO" Then ' Electricity Status

                    iqry += "insert into #DeactCtrlRpt(PROP_ID, Utility, Ageing) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("PROP_ID") & "','Elec','" & dtb.Rows(i).Item("Actual Aging") & "')" & vbCrLf
                End If

                If IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) = "Out of REO" Then ' Water Status

                    iqry += "insert into #DeactCtrlRpt(PROP_ID, Utility, Ageing) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("PROP_ID") & "','Water','" & dtb.Rows(i).Item("Actual Aging") & "')" & vbCrLf
                End If

                If IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) = "Out of REO" Then ' Gas Status

                    iqry += "insert into #DeactCtrlRpt(PROP_ID, Utility, Ageing) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("PROP_ID") & "','Gas','" & dtb.Rows(i).Item("Actual Aging") & "')" & vbCrLf
                End If

                Console.WriteLine(iqry)
                'Console.ReadLine()

                finqry += iqry
            Next

            finqry += "update tbl_UC_PropertyDeact set UpdatedVMS = '0', VMSPostedDate = null from tbl_UC_PropertyDeact a inner join " & vbCrLf
            finqry += "(select a.PROP_ID, a.Utility from #DeactCtrlRpt a left join tbl_uc_openinventory b on a.PROP_ID = b.property_code " & vbCrLf
            finqry += "left join tbl_UC_PropertyDeact c on a.PROP_ID = c.[Property ID] and a.Utility = c.Utility " & vbCrLf
            finqry += "where b.clientcode  = 'OLSR' and c.Status = 'Deactivated' and c.UpdatedVMS <> '3') b on a.[Property ID] = b.PROP_ID and a.Utility = b.Utility " & vbCrLf & vbCrLf

            finqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo)" & vbCrLf
            finqry += "select case when a.Utility = 'Gas' then b.[Gas USP] when a.Utility = 'Elec' then b.[Elec USP] when a.Utility = 'Water' then b.[Water USP] end USP, (select top 1 Timezone from tbl_PPI_timezone where StateCode = b.StateCode) [Time Zone]," & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas USP Contact] when a.Utility = 'Elec' then b.[Elec USP Contact] when a.Utility = 'Water' then b.[Water USP Contact] end [Phone Number]," & vbCrLf
            finqry += "a.PROP_ID [Property ID], case when a.Utility = 'Gas' then b.[Gas Account #] when a.Utility = 'Elec' then b.[Elec Account #] when a.Utility = 'Water' then b.[Water Account #] end [Account #]," & vbCrLf
            finqry += "b.full_address [Address], 'REO' [Category], 'Deactivation' [To Do], a.Utility, case when a.Utility = 'Gas' then b.[Gas Status] when a.Utility = 'Elec' then b.[Elec Status] when a.Utility = 'Water' then b.[Water Status] end [Disposition]," & vbCrLf
            finqry += "case when a.Ageing > 30 then 'Beyond' else 'Within' end SLA, a.Ageing, case when a.Utility = 'Gas' then b.[Gas Vendor ID] when a.Utility = 'Elec' then b.[Elec Vendor ID] when a.Utility = 'Water' then b.[Water Vendor ID] end [vendor ID], b.Zip_Code, cast(GETDATE() as date) DateUploaded, " & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas Meter #] when a.Utility = 'Elec' then b.[Elec Meter #] when a.Utility = 'Water' then b.[Water Meter #] end [Meter Number] " & vbCrLf
            finqry += "from #DeactCtrlRpt a left join tbl_uc_openinventory b on a.PROP_ID = b.property_code left join tbl_UC_PropertyDeact c on a.PROP_ID = c.[Property ID] and a.Utility = c.Utility where b.clientcode  = 'OLSR' and c.[Property ID] IS NULL and c.Utility is null" & vbCrLf

        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while building the query for Deactivation Inflow." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery(finqry)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query for Deactivation Inflow " & vbCrLf & "{" & finqry & "}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            fdt = cls.GetData("select count(*) from tbl_UC_PropertyInFlow where Task = 'Deactivation' and cast(DateUploaded as date) = cast(GETDATE() as date)") '& System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time")).Date & "'")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query " & vbCrLf & "{select count(*) from tbl_UC_PropertyInFlow where Task = 'Deactivation' and cast(DateUploaded as date) = cast(GETDATE() as date)}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine(fdt.Rows(0).Item(0).ToString & " records created...")
        deact = fdt.Rows(0).Item(0)

        'Console.ReadLine()

        'ExportExcel_Deact()
    End Sub

    Sub GetCtrlReport_WP()

        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            dta = New OleDbDataAdapter("Select * From [Water Open Inventory$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to Utility Control Report " & CtrlRptFilename & ".xlsx/Water Open Inventory" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[Water Open Inventory$]")
            dtb = dts.Tables(0)

            'DataGridView1.DataSource = dts.Tables(0)
            'Label1.Text = DataGridView1.Rows.Count
            conn.Close()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from Utility Control Report " & CtrlRptFilename & ".xlsx/Water Open Inventory" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("UC Utility Ctrol Report: " & dts.Tables(0).Rows.Count.ToString & " records found for Water Payout...")
        'Console.ReadLine()

        Dim cls As New clsConnection
        Dim iqry As String = ""
        Dim finqry As String = ""

        Console.WriteLine("Creating the query..")
        'Console.ReadLine()

        finqry += "BEGIN TRY drop table  #WPCtrlRpt END TRY BEGIN CATCH print 'ok' END CATCH " & vbCrLf

        finqry += "select * into #WPCtrlRpt from (select '                    ' PROP_ID, '     ' Ageing) a" & vbCrLf & vbCrLf
        finqry += "delete from #WPCtrlRpt " & vbCrLf & vbCrLf

        Console.WriteLine(finqry)
        'Console.ReadLine()

        Try
            For i As Integer = 0 To dtb.Rows.Count - 1

                iqry = ""

                If IIf(IsDBNull(dtb.Rows(i).Item("Status 2")), "", dtb.Rows(i).Item("Status 2")) = "Open" Then

                    iqry += "insert into #WPCtrlRpt(PROP_ID, Ageing) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("PropertyID") & "','" & dtb.Rows(i).Item(14) & "')" & vbCrLf
                End If

                Console.WriteLine(iqry)
                'Console.ReadLine()

                finqry += iqry
            Next

            finqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo)" & vbCrLf
            finqry += "select b.[Water USP] USP, (select top 1 Timezone from tbl_PPI_timezone where StateCode = b.StateCode) [Time Zone]," & vbCrLf
            finqry += "b.[Water USP Contact] [Phone Number]," & vbCrLf
            finqry += "a.PROP_ID [Property ID], b.[Water Account #] [Account #]," & vbCrLf
            finqry += "b.full_address [Address], 'REO' [Category], 'Water Payout' [To Do], 'Water', b.[Water Status] [Disposition]," & vbCrLf
            finqry += "case when a.Ageing > 30 then 'Beyond' else 'Within' end SLA, a.Ageing, b.[Water Vendor ID] [vendor ID], b.Zip_Code, cast(GETDATE() as date) DateUploaded," & vbCrLf
            finqry += "b.[Water Meter #] [Meter Number] " & vbCrLf
            finqry += "from #WPCtrlRpt a left join tbl_uc_openinventory b on a.PROP_ID = b.property_code left join tbl_UC_WaterPayout c on a.PROP_ID = c.[Property ID] where b.clientcode  = 'OLSR' and c.[Property ID] IS NULL" & vbCrLf
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while building the query for WP Inflow." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery(finqry)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query for WP Inflow " & vbCrLf & "{" & finqry & "}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            fdt = cls.GetData("select count(*) from tbl_UC_PropertyInFlow where Task = 'Water Payout' and cast(DateUploaded as date) = cast(GETDATE() as date)") '& System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time")).Date & "'")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query " & vbCrLf & "{select count(*) from tbl_UC_PropertyInFlow where Task = 'Water Payout' and cast(DateUploaded as date) = cast(GETDATE() as date)}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine(fdt.Rows(0).Item(0).ToString & " records created...")
        wp = fdt.Rows(0).Item(0)
        'Console.ReadLine()

        'ExportExcel_Deact()
    End Sub

    Sub GetCtrlReport_Activation_REO()

        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try

            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            dta = New OleDbDataAdapter("Select * From [Open Inventory - Current Day$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to Utility Control Report " & CtrlRptFilename & ".xlsx/Open Inventory - Current Day" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[Open Inventory - Current Day$]")
            dtb = dts.Tables(0)

            'DataGridView1.DataSource = dts.Tables(0)
            'Label1.Text = DataGridView1.Rows.Count
            conn.Close()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from Utility Control Report " & CtrlRptFilename & ".xlsx/Open Inventory - Current Day" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("UC Utility Ctrol Report: " & dts.Tables(0).Rows.Count.ToString & " records found for Activation (REO)...")
        'Console.ReadLine()

        Dim cls As New clsConnection
        Dim iqry As String = ""
        Dim finqry As String = ""

        Console.WriteLine("Creating the query..")
        'Console.ReadLine()

        finqry += "BEGIN TRY drop table  #ActREOCtrlRpt END TRY BEGIN CATCH print 'ok' END CATCH " & vbCrLf

        finqry += "select * into #ActREOCtrlRpt from (select '                    ' [Property ID], '     ' Utility, '     ' Aging, '          ' SLA, '                                                  ' Disposition) a" & vbCrLf & vbCrLf
        finqry += "delete from #ActREOCtrlRpt " & vbCrLf & vbCrLf

        Console.WriteLine(finqry)
        'Console.ReadLine()

        Try
            For i As Integer = 0 To dtb.Rows.Count - 1

                iqry = ""

                If IIf(IsDBNull(dtb.Rows(i).Item(20)), "", dtb.Rows(i).Item(20)) = "Open" Then ' Electricity Status

                    iqry += "insert into #ActREOCtrlRpt([Property ID], Utility, Aging, SLA, Disposition) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item(0) & "','Elec','" & dtb.Rows(i).Item(25) & "', '" & dtb.Rows(i).Item(26) & "','" & dtb.Rows(i).Item(32) & "')" & vbCrLf
                End If

                If IIf(IsDBNull(dtb.Rows(i).Item(22)), "", dtb.Rows(i).Item(22)) = "Open" Then ' Gas Status

                    iqry += "insert into #ActREOCtrlRpt([Property ID], Utility, Aging, SLA, Disposition) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item(0) & "','Gas','" & dtb.Rows(i).Item(25) & "', '" & dtb.Rows(i).Item(26) & "','" & dtb.Rows(i).Item(33) & "')" & vbCrLf
                End If

                Console.WriteLine(iqry)
                'Console.ReadLine()

                finqry += iqry
            Next

            finqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo)" & vbCrLf
            finqry += "select case when a.Utility = 'Gas' then b.[Gas USP] when a.Utility = 'Elec' then b.[Elec USP] end USP, (select top 1 Timezone from tbl_PPI_timezone where StateCode = b.StateCode) [Time Zone], " & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas USP Contact] when a.Utility = 'Elec' then b.[Elec USP Contact] end [Phone Number]," & vbCrLf
            finqry += "a.[Property ID], case when a.Utility = 'Gas' then b.[Gas Account #] when a.Utility = 'Elec' then b.[Elec Account #] end [Account #], b.full_address [Address], 'REO' [Category], 'Activation' [To Do], a.Utility," & vbCrLf
            finqry += "a.Disposition, -- case when a.Utility = 'Gas' then b.[Gas Last Update] when a.Utility = 'Elec' then b.[Elec Last Update] end [Date Tagged]," & vbCrLf
            finqry += "--case when a.Utility = 'Gas' then b.[Gas Work Item #] when a.Utility = 'Elec' then b.[Elec Work Item #] end [WI #], " & vbCrLf
            finqry += "a.SLA, A.Aging, case when a.Utility = 'Gas' then b.[Gas Vendor ID] when a.Utility = 'Elec' then b.[Elec Vendor ID] end [vendor ID], b.Zip_Code, cast(GETDATE() as date) DateUploaded," & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas Meter #] when a.Utility = 'Elec' then b.[Elec Meter #] when a.Utility = 'Water' then b.[Water Meter #] end [Meter Number] " & vbCrLf
            finqry += "from #ActREOCtrlRpt a left join tbl_uc_openinventory b on a.[Property ID] = b.property_code left join tbl_UC_Activation c on a.[Property ID] = c.PropertyCode and a.Utility = c.Utility and 'REO' = c.Category where c.PropertyCode is null and c.Utility is null" & vbCrLf
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while building the query for Activation (REO) Inflow." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery(finqry)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query for Activation (REO) Inflow " & vbCrLf & "{" & finqry & "}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            fdt = cls.GetData("select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'REO' and cast(DateUploaded as date) = cast(GETDATE() as date)") '& System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time")).Date & "'")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query " & vbCrLf & "{select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'REO' and cast(DateUploaded as date) = cast(GETDATE() as date)}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine(fdt.Rows(0).Item(0).ToString & " records created...")
        act_REO = fdt.Rows(0).Item(0)
        'Console.ReadLine()

        'ExportExcel_Activation()
    End Sub

    Sub GetCtrlReport_Activation_PFC()

        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try

            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";")
            dta = New OleDbDataAdapter("Select [Occupancy Status], [Client Status], [Does the property have a Sump Pump?], IIf(IsNull([Elec Status]), Null, CStr([Elec Status])) as [Elec Status], IIf(IsNull([Water Status]), Null, CStr([Water Status])) as [Water Status], [Property ID], [Aging]  From [PFC Sump Pump$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to Utility Control Report " & CtrlRptFilename & ".xlsx/PFC Sump Pump" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[PFC Sump Pump$]")
            dtb = dts.Tables(0)

            'DataGridView1.DataSource = dts.Tables(0)
            'Label1.Text = DataGridView1.Rows.Count
            conn.Close()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from Utility Control Report " & CtrlRptFilename & ".xlsx/PFC Sump Pump" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("UC Utility Ctrol Report: " & dts.Tables(0).Rows.Count.ToString & " records found for Activation (PFC)...")
        'Console.ReadLine()

        Dim cls As New clsConnection
        Dim iqry As String = ""
        Dim finqry As String = ""

        Console.WriteLine("Creating the query..")
        'Console.ReadLine()

        finqry += "BEGIN TRY drop table  #ActPFCCtrlRpt END TRY BEGIN CATCH print 'ok' END CATCH " & vbCrLf

        finqry += "select * into #ActPFCCtrlRpt from (select '                    ' [Property ID], '     ' Utility, '     ' Aging, '                                                  ' Disposition) a" & vbCrLf & vbCrLf
        finqry += "delete from #ActPFCCtrlRpt " & vbCrLf & vbCrLf

        Console.WriteLine(finqry)
        'Console.ReadLine()

        Try
            For i As Integer = 0 To dtb.Rows.Count - 1

                iqry = ""



                If IIf(IsDBNull(dtb.Rows(i).Item("Occupancy Status")), "", dtb.Rows(i).Item("Occupancy Status")) = "Vacant" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Client Status")), "", dtb.Rows(i).Item("Client Status")) = "Secured Preserve" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Does the property have a Sump Pump?")), "", dtb.Rows(i).Item("Does the property have a Sump Pump?")) = "Yes" Then

                    ' Elec Status

                    If IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "On: Utility turned on" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception - HOA Managed" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception - Well Water" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception - All Electric" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception - Profane Fuel Oil" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) <> "Exception - Deeds Required" Then

                        iqry += "insert into #ActPFCCtrlRpt([Property ID], Utility, Aging, Disposition) " & vbCrLf
                        iqry += "values("
                        iqry += "'" & dtb.Rows(i).Item("Property ID") & "','Elec','" & dtb.Rows(i).Item("Aging") & "','" & IIf(IsDBNull(dtb.Rows(i).Item("Elec Status")), "", dtb.Rows(i).Item("Elec Status")) & "')" & vbCrLf
                    End If

                    ' Water Status

                    If IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "On: Utility turned on" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "On: To be transferred" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Off: Utility turned Off" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - All Electric" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - HOA Managed" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Profane Fuel Oil" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Sold As Is" And _
                        IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Well Water" Then

                        iqry += "insert into #ActPFCCtrlRpt([Property ID], Utility, Aging, Disposition) " & vbCrLf
                        iqry += "values("
                        iqry += "'" & dtb.Rows(i).Item("Property ID") & "','Water','" & dtb.Rows(i).Item("Aging") & "','" & IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) & "')" & vbCrLf
                    End If
                End If

                Console.WriteLine(iqry)
                'Console.ReadLine()

                finqry += iqry
            Next

            finqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo)" & vbCrLf
            finqry += "select b.[Elec USP] USP, (select top 1 Timezone from tbl_PPI_timezone where StateCode = b.StateCode) [Time Zone], " & vbCrLf
            finqry += "case when a.Utility = 'Elec' then b.[Elec USP Contact] else b.[Water USP Contact] end [Phone Number], a.[Property ID], " & vbCrLf
            finqry += "case when a.Utility = 'Elec' then b.[Water Account #] else b.[Water Account #] end, b.full_address [Address], 'PFC' [Category], " & vbCrLf
            finqry += "'Activation' [To Do], a.Utility, a.Disposition, NULL SLA, A.Aging, case when a.Utility = 'Elec' then b.[Elec Vendor ID] else b.[Water Vendor ID] end [vendor ID], " & vbCrLf
            finqry += "b.Zip_Code, cast(GETDATE() as date) DateUploaded, case when a.Utility = 'Elec' then b.[Elec Meter #] else b.[Water Meter #] end [Meter Number] " & vbCrLf
            finqry += "from #ActPFCCtrlRpt a left join tbl_uc_openinventory b on a.[Property ID] = b.property_code left join tbl_UC_Activation c on a.[Property ID] = c.PropertyCode " & vbCrLf
            finqry += "and a.Utility = c.Utility and 'PFC' = c.Category where b.ClientCode = 'PFC' and c.PropertyCode is null and c.Utility is null" & vbCrLf
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while building the query for Activation (PFC) Inflow." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery(finqry)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query for Activation (PFC) Inflow " & vbCrLf & "{" & finqry & "}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            fdt = cls.GetData("select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'PFC' and cast(DateUploaded as date) = cast(GETDATE() as date)") '& System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time")).Date & "'")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query " & vbCrLf & "{select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'PFC' and cast(DateUploaded as date) = cast(GETDATE() as date)}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine(fdt.Rows(0).Item(0).ToString & " records created...")
        act_PFC = fdt.Rows(0).Item(0)

        'My.Computer.FileSystem.DeleteFile("D:\Utility Control Report " & CtrlRptFilename & "_1.xlsx")

        'Console.ReadLine()

        'ExportExcel_Activation()
    End Sub

    Sub GetCtrlReport_Activation_RESI()

        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try

            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\Utility Control Report " & CtrlRptFilename & ".xlsx;Extended Properties=Excel 12.0;")
            dta = New OleDbDataAdapter("Select * From [RESI-Open Inventory$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to Utility Control Report " & CtrlRptFilename & ".xlsx/RESI-Open Inventory" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            dta.Fill(dts, "[RESI-Open Inventory$]")
            dtb = dts.Tables(0)

            'DataGridView1.DataSource = dts.Tables(0)
            'Label1.Text = DataGridView1.Rows.Count
            conn.Close()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from Utility Control Report " & CtrlRptFilename & ".xlsx/RESI-Open Inventory" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine("UC Utility Ctrol Report: " & dts.Tables(0).Rows.Count.ToString & " records found for Activation (RESI)...")
        'Console.ReadLine()

        Dim cls As New clsConnection
        Dim iqry As String = ""
        Dim finqry As String = ""

        Console.WriteLine("Creating the query..")
        'Console.ReadLine()

        finqry += "BEGIN TRY drop table  #ActRESICtrlRpt END TRY BEGIN CATCH print 'ok' END CATCH " & vbCrLf

        finqry += "select * into #ActRESICtrlRpt from (select '                    ' [Property ID], '     ' Utility, '     ' Aging, '                                                  ' Disposition) a" & vbCrLf & vbCrLf
        finqry += "delete from #ActRESICtrlRpt " & vbCrLf & vbCrLf

        Console.WriteLine(finqry)
        'Console.ReadLine()

        Try
            For i As Integer = 0 To dtb.Rows.Count - 1

                iqry = ""

                ' Electricity Status
                If IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "On: Utility turned on" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception - HOA Managed" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception - Well Water" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception - All Electric" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception - Profane Fuel Oil" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) <> "Exception - Deeds Required" Then

                    iqry += "insert into #ActRESICtrlRpt([Property ID], Utility, Aging, Disposition) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("Property ID") & "','Elec','" & dtb.Rows(i).Item("Aging") & "','" & IIf(IsDBNull(dtb.Rows(i).Item("Electric Status")), "", dtb.Rows(i).Item("Electric Status")) & "')" & vbCrLf
                End If

                ' Water Status
                If IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "On: Utility turned on" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - HOA Managed" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Well Water" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - All Electric" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Profane Fuel Oil" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Exception - Deeds Required" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) <> "Off: Utility turned off" Then

                    iqry += "insert into #ActRESICtrlRpt([Property ID], Utility, Aging, Disposition) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("Property ID") & "','Water','" & dtb.Rows(i).Item("Aging") & "','" & IIf(IsDBNull(dtb.Rows(i).Item("Water Status")), "", dtb.Rows(i).Item("Water Status")) & "')" & vbCrLf
                End If


                ' Gas Status
                If IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "On: Utility turned on" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception - HOA Managed" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception - Well Water" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception - All Electric" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception - Profane Fuel Oil" And _
                    IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) <> "Exception - Deeds Required" Then

                    iqry += "insert into #ActRESICtrlRpt([Property ID], Utility, Aging, Disposition) " & vbCrLf
                    iqry += "values("
                    iqry += "'" & dtb.Rows(i).Item("Property ID") & "','Gas','" & dtb.Rows(i).Item("Aging") & "','" & IIf(IsDBNull(dtb.Rows(i).Item("Gas Status")), "", dtb.Rows(i).Item("Gas Status")) & "')" & vbCrLf
                End If

                Console.WriteLine(iqry)
                'Console.ReadLine()

                finqry += iqry
            Next

            finqry += "insert into tbl_UC_PropertyInFlow(USPName, TimeZone, PhoneNo, PropertyCode, AccountNo, Address, Category, Task, Utility, Status, SLA, Ageing, VendorID, ZipCode, DateUploaded, MeterNo)" & vbCrLf
            finqry += "select case when a.Utility = 'Gas' then b.[Gas USP] when a.Utility = 'Elec' then b.[Elec USP] when a.Utility = 'Water' then b.[Water USP] end USP, (select top 1 Timezone from tbl_PPI_timezone where StateCode = b.StateCode) [Time Zone], " & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas USP Contact] when a.Utility = 'Elec' then b.[Elec USP Contact] when a.Utility = 'Water' then b.[Water USP Contact] end [Phone Number]," & vbCrLf
            finqry += "a.[Property ID], case when a.Utility = 'Gas' then b.[Gas Account #] when a.Utility = 'Elec' then b.[Elec Account #] when a.Utility = 'Water' then b.[Water Account #] end [Account #], b.full_address [Address], 'RESI' [Category], 'Activation' [To Do], a.Utility," & vbCrLf
            finqry += "a.Disposition, NULL SLA, A.Aging, case when a.Utility = 'Gas' then b.[Gas Vendor ID] when a.Utility = 'Elec' then b.[Elec Vendor ID] when a.Utility = 'Water' then b.[Water Vendor ID] end [vendor ID], b.Zip_Code, cast(GETDATE() as date) DateUploaded, " & vbCrLf
            finqry += "case when a.Utility = 'Gas' then b.[Gas Meter #] when a.Utility = 'Elec' then b.[Elec Meter #] when a.Utility = 'Water' then b.[Water Meter #] end [Meter Number]  " & vbCrLf
            finqry += "from #ActRESICtrlRpt a left join tbl_uc_openinventory b on a.[Property ID] = b.property_code " & vbCrLf
            finqry += "left join tbl_UC_Activation c on a.[Property ID] = c.PropertyCode and a.Utility = c.Utility and 'RESI' = c.Category" & vbCrLf
            finqry += "where b.ClientCode = 'RESI'  and c.PropertyCode is null and c.Utility is null" & vbCrLf

        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while building the query for Activation (RESI) Inflow." & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            cls.ExecuteQuery(finqry)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query for Activation (RESI) Inflow " & vbCrLf & "{" & finqry & "}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Try
            fdt = cls.GetData("select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'RESI' and cast(DateUploaded as date) = cast(GETDATE() as date)") '& System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time")).Date & "'")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query " & vbCrLf & "{select count(*) from tbl_UC_PropertyInFlow where Task = 'Activation' and Category = 'RESI' and cast(DateUploaded as date) = cast(GETDATE() as date)}" & vbCrLf & ex.ToString)
            Exit Sub
        End Try

        Console.WriteLine(fdt.Rows(0).Item(0).ToString & " records created...")
        act_RESI = fdt.Rows(0).Item(0)
        'Console.ReadLine()

        'ExportExcel_Activation()
    End Sub

    Sub GetOpenReport_OLSR()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #] From [OLSR$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try


        Try
            dta.Fill(dts, "[OLSR$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for OLSR." & vbCrLf & ex.ToString)
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for OLSR to the database." & vbCrLf & ex.ToString)
        End Try

        Try
            cls.ExecuteQuery("delete from tbl_UC_OpenInventory where property_code is null")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
        End Try

        Console.WriteLine("OLSR Import finished...")
        'Console.ReadLine()
    End Sub

    Sub GetOpenReport_PFC()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try

            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #] From [PFC$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for PFC." & vbCrLf & ex.ToString)
        End Try

        Try
            dta.Fill(dts, "[PFC$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for PFC." & vbCrLf & ex.ToString)
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for PFC to the database." & vbCrLf & ex.ToString)
        End Try

        Try
            cls.ExecuteQuery("delete from tbl_UC_OpenInventory where property_code is null")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
        End Try

        Console.WriteLine("PFC Import finished...")
        'Console.ReadLine()
    End Sub

    Sub GetOpenReport_RESI()

        Dim cls As New clsConnection
        Dim conn As OleDbConnection
        Dim dta As OleDbDataAdapter
        Dim dts As DataSet
        Dim dtb As DataTable

        Try
            conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            'conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\elayronj\Documents\UC Open Report.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES;';")
            dta = New OleDbDataAdapter("Select property_code, [Gas Account #], [Gas USP Contact], [Gas USP], [Gas Status], [Elec Account #], [Elec USP Contact], [Elec USP], [Elec Status], [Water Account #], [Water USP Contact], [Water USP], [Water Status], full_address, state_short_name, [Gas Vendor ID], [Gas Last Update], [Gas Work Item #], [Elec Vendor ID], [Elec Last Update], [Elec Work Item #], [Water Vendor ID], [Water Last Update], [Water Work Item #], ClientCode, zip_code, [Gas Meter #], [Elec Meter #], [Water Meter #] From [RESI$]", conn)
            dts = New DataSet
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while connecting to UC Open Report.xlsx for RESI." & vbCrLf & ex.ToString)
        End Try

        Try
            dta.Fill(dts, "[RESI$]")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while retrieving the data from UC Open Report.xlsx for RESI." & vbCrLf & ex.ToString)
        End Try

        dtb = dts.Tables(0)
        cnt = dtb.Rows.Count
        'DataGridView2.DataSource = dts.Tables(0)
        'Label2.Text = DataGridView2.Rows.Count
        conn.Close()

        Console.WriteLine("Open Report: " & dts.Tables(0).Rows.Count.ToString & " records found.")
        'Console.ReadLine()

        Try
            cls.SQLBulkCopy("tbl_UC_OpenInventory", dtb)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the data from UC Open Report.xlsx for RESI to the database." & vbCrLf & ex.ToString)
        End Try

        Try
            cls.ExecuteQuery("delete from tbl_UC_OpenInventory where property_code is null")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory where property_code is null}" & vbCrLf & ex.ToString)
        End Try

        Console.WriteLine("RESI Import finished...")
        'Console.ReadLine()
    End Sub

    Sub FormatCells()

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As New Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        xlApp = New Excel.ApplicationClass

        Try
            xlWorkBook = xlApp.Workbooks.Open("D:\UC Open Report.xlsx")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "Unable to access UC Open Report.xlsx." & vbCrLf & ex.ToString)
        End Try

        Try
            For i = 1 To 3

                If i = 1 Then
                    xlWorkSheet = xlWorkBook.Worksheets("OLSR")
                ElseIf i = 2 Then
                    xlWorkSheet = xlWorkBook.Worksheets("RESI")
                Else
                    xlWorkSheet = xlWorkBook.Worksheets("PFC")
                End If

                xlWorkSheet.Range("k1", "k108756").NumberFormat = "######################" 'Gas Account #
                xlWorkSheet.Range("w1", "w108756").NumberFormat = "######################" 'Elec Account #
                xlWorkSheet.Range("ai1", "ai108756").NumberFormat = "######################" 'Water Account #
                xlWorkSheet.Range("l1", "l108756").NumberFormat = "################" 'Gas USP Contact
                xlWorkSheet.Range("x1", "x108756").NumberFormat = "################" 'Elec USP Contact
                xlWorkSheet.Range("aj1", "aj108756").NumberFormat = "################" 'Water USP Contact
                xlWorkSheet.Range("i1", "i108756").NumberFormat = "################" 'Gas Vendor ID
                xlWorkSheet.Range("u1", "u108756").NumberFormat = "################" 'Elec Vendor ID
                xlWorkSheet.Range("ag1", "ag108756").NumberFormat = "################" 'Water Vendor ID
                xlWorkSheet.Range("j1", "j108756").NumberFormat = "######################" 'Gas Meter #
                xlWorkSheet.Range("v1", "v108756").NumberFormat = "######################" 'Elec Meter #
                xlWorkSheet.Range("ah1", "ah108756").NumberFormat = "######################" 'Water Meter #
                xlWorkSheet.Range("n1", "n108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Gas Last Update
                xlWorkSheet.Range("r1", "r108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Gas WI Issued Date
                xlWorkSheet.Range("s1", "s108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Gas WI Resolution Date
                xlWorkSheet.Range("z1", "z108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Elec Last Update
                xlWorkSheet.Range("ad1", "ad108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Elec WI Issued Date
                xlWorkSheet.Range("ae1", "ae108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Elec WI Resolution Date
                xlWorkSheet.Range("al1", "al108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Water Last Update
                xlWorkSheet.Range("ap1", "ap108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Water WI Issued Date
                xlWorkSheet.Range("aq1", "aq108756").Replace("NULL", "1900/01/01", 2, 1, False, False, False, misValue) 'Water WI Resolution Date
                xlWorkSheet.Range("ar1", "ar1").Value = "ClientCode"
                xlWorkSheet.Range("as1", "as1").Value = "DateUploaded"

                If i = 1 Then
                    xlWorkSheet.Range("ar2", "ar108756").Value = "OLSR"
                    xlWorkSheet.Range("as2", "as108756").Value = Now().Date
                ElseIf i = 2 Then
                    xlWorkSheet.Range("ar2", "ar108756").Value = "RESI"
                    xlWorkSheet.Range("as2", "as108756").Value = Now().Date
                Else
                    xlWorkSheet.Range("ar2", "ar108756").Value = "PFC"
                    xlWorkSheet.Range("as2", "as108756").Value = Now().Date
                End If
            Next
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while formatting UC Open Report.xlsx. A certain data could not be formatted or the fields of the file might have changed." & vbCrLf & ex.ToString)
        End Try

        Try
            xlWorkBook.Save()
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the UC Open Report.xlsx." & vbCrLf & ex.ToString)
        End Try

        Try
            xlWorkBook.Close(True, "UC Open Report", misValue)
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while closing the UC Open Report.xlsx." & vbCrLf & ex.ToString)
        End Try
    End Sub

    Sub FormatCells2()

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As New Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        xlApp = New Excel.ApplicationClass

        Try
            xlWorkBook = xlApp.Workbooks.Open("D:\Utility Control Report " & CtrlRptFilename & ".xlsx", False, True)
            xlWorkSheet = xlWorkBook.Worksheets("PFC Sump Pump")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "Unable to access Utility Control Report " & CtrlRptFilename & ".xlsx." & vbCrLf & ex.ToString)
        End Try

        Try

            xlWorkSheet.Range("s1", "s108756").NumberFormat = "@" ' Elec Status
            xlWorkSheet.Range("w1", "w108756").NumberFormat = "@" ' Water Status

        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while formatting Utility Control Report " & CtrlRptFilename & ".xlsx. A certain data could not be formatted or the fields of the file might have changed." & vbCrLf & ex.ToString)
        End Try

        Try
            xlWorkBook.SaveAs("D:\Utility Control Report " & CtrlRptFilename & "_1.xlsx")
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while saving the Utility Control Report " & CtrlRptFilename & ".xlsx." & vbCrLf & ex.ToString)
        End Try

        Try
            xlWorkBook.Close(True, "Utility Control Report " & CtrlRptFilename, misValue)
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        Catch ex As Exception
            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while closing the Utility Control Report " & CtrlRptFilename & ".xlsx." & vbCrLf & ex.ToString)
        End Try
    End Sub

    Sub DeleteFRow()

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As New Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        xlApp = New Excel.ApplicationClass

        xlWorkBook = xlApp.Workbooks.Open("D:\Utility Control Report " & Format(DateAdd(DateInterval.Day, -1, Now().Date), "MMddyyyy") & ".xlsx", misValue, True)
        'xlWorkBook = xlApp.Workbooks.Open("C:\Users\elayronj\Documents\Utility Control Report " & Format(DateAdd(DateInterval.Day, -1, Now().Date), "MMddyyyy") & ".xlsx", misValue, True)
        xlWorkSheet = xlWorkBook.Worksheets("Open Inventory - Current Day")

        xlWorkSheet.Rows(1).Delete()
        xlWorkBook.Save()
        xlWorkBook.Close(True, "Utility Control Report " & Format(DateAdd(DateInterval.Day, -1, Now().Date), "MMddyyyy"), misValue)
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
    End Sub

    Sub Main()

        Dim task As String
        Dim CtrlFile As String
        Dim OpenRptFile As String
        Dim CtrlRptFolder As String = ""
        Dim nDate As Date = Now().Date
        Dim cls As New clsConnection

        Console.WriteLine("Enter the type of Inflow using the letter code below...")
        Console.WriteLine("[A] - Deactivation")
        Console.WriteLine("[B] - Water Payout")
        Console.WriteLine("[C] - Activation (REO)")
        Console.WriteLine("[D] - Activation (PFC)")
        Console.WriteLine("[E] - Activation (RESI)")
        Console.WriteLine("[F] - All")
        Console.WriteLine()
        task = Console.ReadLine()
        task = task.ToUpper

        Do

            If task IsNot Nothing And (task = "A" Or task = "B" Or task = "C" Or task = "D" Or task = "E" Or task = "F") Then
                Exit Do
            Else
                Console.WriteLine("You entered an incorrect task.")
                Console.WriteLine("Enter the type of Inflow using the letter code below...")
                Console.WriteLine("[A] - Deactivation")
                Console.WriteLine("[B] - Water Payout")
                Console.WriteLine("[C] - Activation (REO)")
                Console.WriteLine("[D] - Activation (PFC)")
                Console.WriteLine("[E] - Activation (RESI)")
                Console.WriteLine("[F] - All")
                Console.WriteLine()
                task = Console.ReadLine()
                task = task.ToUpper
            End If
        Loop While task IsNot Nothing And (task <> "A" Or task <> "B" Or task <> "S" Or task <> "D" Or task <> "E" Or task <> "F")

        deact = 0
        wp = 0
        act_REO = 0
        act_PFC = 0
        act_RESI = 0

        CtrlRptFilename = IIf(Weekday(nDate) = "2", Format(DateAdd(DateInterval.Day, -3, nDate.Date), "MMddyyyy"), Format(DateAdd(DateInterval.Day, -1, nDate.Date), "MMddyyyy"))
        CtrlRptFolder = Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -3, nDate.Date), DateAdd(DateInterval.Day, -1, nDate.Date)), "MMM") & "-" & Format(IIf(Weekday(nDate) = "2", DateAdd(DateInterval.Day, -3, nDate.Date), DateAdd(DateInterval.Day, -1, nDate.Date)), "yy")
        CtrlFile = "\\bpv12fcocmp01\strategic$\Internal\Operations\PPI-MIS\Internal\Reports - REO\Utility Reports\" & CtrlRptFolder & "\Utility Control Report " & CtrlRptFilename & ".xlsx"
        OpenRptFile = "\\bpv12fcocmp01\strategic$\Manila Schedule\UC Open Report.xlsx"

        If File.Exists(CtrlFile) And System.IO.File.GetLastWriteTime(CtrlFile).ToShortDateString() >= Now().Date Then

            Console.WriteLine("Utility Control Report File found...")

            If File.Exists(OpenRptFile) Then

                If System.IO.File.GetLastWriteTime(OpenRptFile).ToShortDateString() >= Now().Date Then

                    Console.WriteLine("UC Open Report File found...")

                    'Console.WriteLine("Copying files to your local drive...")

                    'Try
                    '    My.Computer.FileSystem.CopyFile(CtrlFile, "D:\Utility Control Report " & CtrlRptFilename & ".xlsx", True)
                    'Catch ex As Exception
                    '    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "Could not copy the file Utility Control Report " & CtrlRptFilename & ".xlsx to your local drive (D:\)" & ex.ToString)
                    'End Try

                    'Try
                    '    My.Computer.FileSystem.CopyFile(OpenRptFile, "D:\UC Open Report.xlsx", True)
                    'Catch ex As Exception
                    '    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "Could not copy the file UC Open Report.xlsx to your local drive (D:\)" & ex.ToString)
                    'End Try

                    'Console.WriteLine("Formatting cells...")
                    'FormatCells()

                    'Try
                    '    cls.ExecuteQuery("delete from tbl_UC_OpenInventory")
                    'Catch ex As Exception
                    '    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "An error occurred while executing the query {delete from tbl_UC_OpenInventory}" & vbCrLf & ex.ToString)
                    'End Try

                    'Console.WriteLine("Importing Open Inventory Report for OLSR...")
                    'GetOpenReport_OLSR()

                    'Console.WriteLine("Importing Open Inventory Report for RESI...")
                    'GetOpenReport_RESI()

                    'Console.WriteLine("Importing Open Inventory Report for PFC...")
                    'GetOpenReport_PFC()

                    If task = "A" Then
                        Console.WriteLine("Importing data for Deactivation from UC Utility Control Report...")
                        GetCtrlReport_Deact()
                    ElseIf task = "B" Then
                        Console.WriteLine("Importing data for Water Payout from UC Utility Control Report...")
                        GetCtrlReport_WP()
                    ElseIf task = "C" Then
                        Console.WriteLine("Importing data for Activation (REO) from UC Utility Control Report...")
                        GetCtrlReport_Activation_REO()
                    ElseIf task = "D" Then
                        Console.WriteLine("Importing data for Activation (PFC) from UC Utility Control Report...")
                        'FormatCells2()
                        GetCtrlReport_Activation_PFC()
                    ElseIf task = "E" Then
                        Console.WriteLine("Importing data for Activation (RESI) from UC Utility Control Report...")
                        GetCtrlReport_Activation_RESI()
                    Else
                        Console.WriteLine("Importing data for Deactivation from UC Utility Control Report...")
                        GetCtrlReport_Deact()

                        Console.WriteLine("Importing data for Water Payout from UC Utility Control Report...")
                        GetCtrlReport_WP()

                        Console.WriteLine("Importing data for Activation (REO) from UC Utility Control Report...")
                        GetCtrlReport_Activation_REO()

                        Console.WriteLine("Importing data for Activation (PFC) from UC Utility Control Report...")
                        'FormatCells2()
                        GetCtrlReport_Activation_PFC()

                        Console.WriteLine("Importing data for Activation (RESI) from UC Utility Control Report...")
                        GetCtrlReport_Activation_RESI()
                    End If

                    UpdateORLastDate(System.IO.File.GetLastWriteTime(OpenRptFile))

                    clsMail.CreateSendEmail(1, deact, wp, act_REO, act_PFC, act_RESI, "Upload Inflow successful.")
                Else
                    Console.WriteLine("UC Open Report's Last Modified Date is " & System.IO.File.GetLastWriteTime(OpenRptFile).ToShortDateString().ToString() & ". File is not updated.")

                    clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, "UC Open Report's Last Modified Date is " & System.IO.File.GetLastWriteTime(OpenRptFile).ToShortDateString().ToString() & ". File is not updated.")
                End If
            Else
                Console.WriteLine("UC Open Report File not found...")

                clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, OpenRptFile & " was not found.")
            End If
        Else
            Console.WriteLine("Utility Control Report File not found...")

            clsMail.CreateSendEmail(0, 0, 0, 0, 0, 0, CtrlFile & " was not found.")
        End If
    End Sub
End Module
