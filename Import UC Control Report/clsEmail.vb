﻿'Imports System.Web.Mail
Imports System.Net.Mail

Public Class clsEmail

    Dim sto As String
    Dim cc As String


    Public Sub CreateSendEmail(ByVal remarks As String, ByVal file_typ As String)

        Dim e_frm, e_body, e_server, e_subj As String
        Dim oMsg As MailMessage = New MailMessage()
        'Dim dt As DataTable = MailDtls()
        Dim val As Integer = 0

        e_frm = "DEV_MNL@altisource.com"
        e_subj = file_typ & " | " & Now().Date

        e_body = ""

        e_server = "Internal-Mail.ascorp.com"

        ' TODO: Replace with sender e-mail address.
        oMsg.From = New MailAddress(e_frm)
        ' TODO: Replace with recipient e-mail address.

        oMsg.To.Add("Richel.Atup@altisource.com")
        oMsg.To.Add("Heherson.Blas@altisource.com")
        oMsg.CC.Add("DEV_MNL@altisource.com")

        oMsg.Subject = e_subj

        ' SEND IN HTML FORMAT (comment this line to send plain text).
        'oMsg.BodyFormat = MailFormat.Html

        oMsg.IsBodyHtml = True

        'If mode = 0 Then
        '    ' ADD AN ATTACHMENT.
        '    ' TODO: Replace with path to attachment.
        '    Dim sFile As String = IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName) & "\Import RESI OCWEN " & Format(Now().Date(), "MMM yyyy") & ".txt"
        '    Dim oAttch As MailAttachment = New MailAttachment(sFile, MailEncoding.Base64)

        '    oMsg.Attachments.Add(oAttch)

        '    'HTML Body (remove HTML tags for plain text).

        '    oMsg.Body = "<HTML><BODY><p><font face=""Calibri"">" & remarks & "</font></p>"
        '    oMsg.Body += "<p><font face=""Calibri"">Please refer to the attached log file.</font></p></BODY></HTML>"
        'Else
        oMsg.Body = "<HTML><BODY><p><font face=""Calibri"">" & remarks & "</font></p></BODY></HTML>" & vbCrLf
        oMsg.Body += "<p><font face=""Calibri"">MIS Manila</font></p>"
        'End If

        ' TODO: Replace with the name of your remote SMTP server.
        'SmtpMail.SmtpServer = e_server

        Dim smtp As New Net.Mail.SmtpClient(e_server)
        smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
        smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

        Try

            smtp.Send(oMsg)

            'SmtpMail.Send(oMsg)
            'Console.WriteLine("Email successfully sent...")
        Catch ex As Exception
            'Console.WriteLine("Email was not sent...")
        End Try

        oMsg = Nothing
    End Sub
End Class
